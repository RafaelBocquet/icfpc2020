{-# LANGUAGE LambdaCase #-}
{-# language BlockArguments, BangPatterns #-}

module Main where

import Debug.Trace

data Val0 = V0Int Integer
          | V0Nil
          | V0Cons Val0 Val0
          deriving (Show, Read)

data Val = VInt Integer
         | VNil
         | VCons Val Val
         | VLam (Val -> Val)

-- disp :: Val -> Int -> String
-- disp (VInt x)    ix = "(" ++ show x ++ ")"
-- disp VNil        ix = "()"
-- disp (VCons x y) ix = "(" ++ disp x ix ++ " " ++ disp y ix ++ ")"
-- disp (VVAR x)    ix = "#" ++ show x
-- disp (VLam x)    ix = "(\\#" ++ show ix ++ ". " ++ " INNER " ++ ")"

toVal0 :: Val -> Val0
toVal0 (VInt x) = V0Int x
toVal0 VNil = V0Nil
toVal0 (VCons x y) = V0Cons (toVal0 x) (toVal0 y)

fromVal0 :: Val0 -> Val
fromVal0 (V0Int x) = VInt x
fromVal0 V0Nil = VNil
fromVal0 (V0Cons x y) = VCons (fromVal0 x) (fromVal0 y)

infixl 8 $$
($$) :: Val -> Val -> Val
-- f $$ a | trace (disp f 0 ++ " --- " ++ disp a 0) False = undefined
VLam f $$ a = f a
VNil $$ a = vtrue
VCons x y $$ a = (a $$ x) $$ y
{-# inline ($$) #-}

vCons :: Val
vCons = VLam (\x -> VLam (\y -> VCons x y))

vsucc :: Val
vsucc = VLam (\(VInt x) -> VInt (x+1))

vpred :: Val
vpred = VLam (\(VInt x) -> VInt (x-1))

vadd :: Val
vadd = VLam (\(VInt x) -> VLam (\(VInt y) -> VInt (x+y)))

vneg :: Val
vneg = VLam (\(VInt x) -> VInt (-x))

vmul :: Val
vmul = VLam (\(VInt x) -> VLam (\(VInt y) -> VInt (x*y)))

vquot :: Val
vquot = VLam (\(VInt x) -> VLam (\(VInt y) -> VInt (x`quot`y)))

vtrue :: Val
vtrue = VLam (\x -> VLam (\_ -> x))

vfalse :: Val
vfalse = VLam (\_ -> VLam (\x -> x))

veq :: Val
veq = VLam (\(VInt x) -> VLam (\(VInt y) -> if x == y then vtrue else vfalse))

vlt :: Val
vlt = VLam (\(VInt x) -> VLam (\(VInt y) -> if x < y then vtrue else vfalse))

vpwr2 :: Val
vpwr2 = VLam (\(VInt x) -> VInt (2^x))

vIsNil :: Val
vIsNil = VLam (\case
                  VNil -> vtrue
                  _ -> vfalse)

vCar :: Val
vCar = VLam (\(VCons x y) -> x)

vCdr :: Val
vCdr = VLam (\(VCons x y) -> y)

vS :: Val
vS = VLam (\x -> VLam (\y -> VLam (\z -> (x $$ z) $$ (y $$ z))))

vC :: Val
vC = VLam (\x -> VLam (\y -> VLam (\z -> (x $$ z) $$ y)))

vB :: Val
vB = VLam (\x -> VLam (\y -> VLam (\z -> x $$ (y $$ z))))

vI :: Val
vI = VLam (\x -> x)

main :: IO ()
main = do
  x <- fromVal0 . read <$> getLine
  y <- fromVal0 . read <$> getLine
  -- print $ toVal0 ((statelessdraw $$ x) $$ y)
  -- print $ toVal0 ((statefuldraw $$ x) $$ y)
  print $ toVal0 ((galaxy $$ x) $$ y)

-- V0Nil
-- V0Cons (V0Int 1) (V0Int 1)
