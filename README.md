# [ICFP Programming Contest 2020](https://icfpcontest2020.github.io/)
## Team "All your ~~lambda~~ galaxy combinator are belong to us"

## Members
- Rafaël Bocquet
- Nathanaël Courant

## Language: C++

## Compilation

``` sh
mkdir -p build/release 
cmake -S . -B build/release -DCMAKE_BUILD_TYPE=Release
```

## Tools

### Interact with the galaxy
``` sh
./build/release/interact --prg data/galaxy.txt --st "()"
```

The interface is written using [Dear ImGui](https://github.com/ocornut/imgui).

![readme_images/Galaxy.png](readme_images/Galaxy.png)

After the contest, we compiled this tool to WebAssembly. It is available [here](https://rafaelbocquet.gitlab.io/contests/icfpc2020/galaxy.html).

### Start a local game

This will display the logs of the two AIs and copy one of the player keys to the clipboard. 
This player key can be used to replay a game.
``` sh
./game.sh
```

### Replay a game

We can replay games in the galaxy evaluator by hacking its state:
``` sh
./replay.sh [PLAYERKEY]
./replay.sh 6595584927974503579 # Example
```

### AI
After the contest, we compiled our final submission to WebAssembly. 
It is available [here](https://rafaelbocquet.gitlab.io/contests/icfpc2020/ai.html).
To play against it, create a game (for instance using ```./build/release/send [1 0]```), and copy one of the player keys in the prompt. 
The other player key can be given to another instance of our ai or to another team's ai.

## General remarks

We liked the contest a lot, both in its theme and the task that was to be done. 
We found that having the whole task specification without any human language was very nice.

There were four hours at the beginning which were quite slow, where we did not have the contents of galaxy, and the objective of the contest was unclear. 
While this allowed to keep the theme, the beginning is typically the moment where we were the least tired, so it was unfortunate that we were kept waiting at that moment.

We wrote two evaluators, one in C++ and one where we compiled galaxy.txt to Haskell. Both started working at approximately the same time, but we only used the C++ one for the remaining of the contest.

We used [Dear ImGui](https://github.com/ocornut/imgui) for creating a GUI; it was very easy to create a non-trivial interface with it.

We liked that the contest was more about time management that just writing an AI: although some people complained about the organizers giving hints, we felt like the objective was rather to keep ahead of the hints and have more time to figure out each part than other teams.

All in all, we really liked this contest, which was in many ways similar to the contests in 2006 and 2007. We feel like this year will join 2006 and 2007 as the best years of ICFP contests.

## Timeline

**Fri 15:00.** The contest starts: the messages #16..#42 are released as images.

**Fri 15:30.** We understand the content of the messages. We clearly can't do much without the definition of the "galaxy". As we expect to receive its definition in further images, we start to write a parser for images.

**Fri 16:15.** We can convert between values (natural numbers and lists and pairs of values) and bit streams in Haskell.

**Fri 17:00.** It becomes likely that the definition of the galaxy won't be given as an image.

**Fri 17:30.** We start to work on a GUI in C++.

**Fri 18:30.** We have a basic GUI, it remains to combine it with a galaxy evaluator.

**Fri 19:00.** An API to send messages encoded as bit streams was made public.

**Fri 19:30.** Galaxy.txt is released.

**Fri 20:20.** We got a working implementation of send in C++. This was quite tedious, because the http library suggested in the cpp-starterkit did not seem handle post requests with both arguments and a body.
We start working on the interpreter, the GUI and the parsing of expressions in parallel.

**Fri 21:40.** We can parse galaxy.txt. Our evaluator is too strict.

**Fri 01:00.** Our evaluator works.

**Fri 01:40.** We score our first point in the lightning round (by clicking randomly). We go to sleep.

**Sat 06:40.** Scrolling and zooming is implemented. We score more points by clicking randomly. Because we only display one at a time, we don't really understand what is going on yet. Our interpreter sometimes crashes after receiving a message.

**Sat 08:30.** The tictactoe puzzle is solved.

**Sat 08:50.** We recognize and display the 2D numbers as decimal numbers. We fix the bug that caused the crashes. We solve the tutorial 3 and 4.
By pure luck, we notice that solving tictactoe gives us a bonus. We use that to improve our score on the tutorials.
We implement a way to record sequences of moves in the GUI.

**Sat 11:00.** We change our team's name from "All your lambda are belong to us" to "All your galaxy combinator are belong to us".

**Sat 12:10.** The memory puzzle is solved. Unforunately, there was a bug in the puzzle, so we don't get the corresponding bonus.
We are unable to solve tutorial 5, and now believe that it cannot be solved in the lightning round.
We were expecting this bonus to be necessary to make progress in the tutorial 5. It was not actually necessary, but it would have allowed us to progress.
At this point, we have 23 points, the other teams only have 3 points. We are confident in the fact that we have explored everything in the galaxy. As we have a huge lead, we are confident in the fact that we won't be overtaken.

**Sat 14:00.** Team "powder" solves tutorial 5. We look at it again.

**Sat 14:[REDACTED].** After [REDACTED] minutes, we solve tutorial 5.

**Sat 15:00.** The lightning round ends. We have more than 27 points (Team "powder"'s score when the leaderboard wsa frozen).

**Sat 15:00-18:00.** We play the tutorials [REDACTED]..12.

**Sat 18:00-24:00.** We reverse engineer the AI protocol, the meaning of the four ship characteristics, the gravity, and write an AI that does not crashes on the planet.

**Sat 21:00.** We write a script to play games locally and replay existing games by hacking the state for the past battles replays.

**Sun 09:00.** We reverse engineer the formulas for explosion damage.
           We also had a look at the formulas for firing damage but are unable to find out the formula, misled by the buggy graphic in the galaxy. 
           
![readme_images/power.png](readme_images/power.png)
![readme_images/power_fixed.png](readme_images/power_fixed.png)

**Sun 10:40.** We brute-force the memory puzzle again, and put all the possible tokens given by the puzzle in our AI.

**Sun 11:20.** We fix a bug in our interpretation of the protocol: we mixed up the "owner" and "ship_id" components when parsing ships.

**Sun 14:00.** Our ships are able to move, and can try to follow or avoid the enemy ship's trajectory.

**Sun 16:00.** We experiment with firing and with cloning ships.

**Sun 17:45.** We decide to always send ships in both clockwise and counterclockwise orbits, in order to intercept the opponent's ships easily. We write functions to move ships to higher and lower orbits.

**Sun 19:30.** We start using explosions.

**Mon 08:50.** We use beam search to find fuel-efficient ways to avoid the opponent's ships when defending.

**Mon 10:00.** We start using explosions for the defender as well, if it allows killing all enemies and some ships survive.

**Mon 10:30.** The change in the maximum number of turns made our AI crash on the planet at the end of games, so we fix this.

**Mon 12:00.** We start implementing "missiles", in order to counter the defensers splitting their ship into many small ships.

**Mon 13:00.** The leaderboard is frozen. We can no longer start games with the AIs of other teams.

**Mon 14:15.** The missiles are working.

**Mon 14:15-15:00.** We make many slightly different submissions in order to compare them.

**Mon 15:30.** We select one of our submissions as our final submission. We were mainly hesitating between using only missiles, or using both lasers and misssiles. We decided to use both.
