with import <nixpkgs> {};
stdenv.mkDerivation rec {
  name = "env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    cmake ninja zlib libGLU gmp boost glfw3 glew
    gcc
    stack
    ocaml (import <unstable> {}).dune_2
  ] ++ (with ocamlPackages; [
    findlib yojson zarith
  ]);
}