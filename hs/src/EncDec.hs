module EncDec where
import Data.Bits
import Data.List

data Value = ValInt Integer | ValCons Value Value | ValNil deriving Show

(|:) = ValCons

decInt 0 l c = (c, l)
decInt _ [] c = error "empty"
decInt k (b : l) c = decInt (k - 1) l (2 * c + if b then 1 else 0)

decLenInt k [] = error "empty"
decLenInt k (True : l) = decLenInt (k + 1) l
decLenInt k (False : l) = decInt (4 * k) l 0

dec [] = error "empty"
dec [_] = error "empty"
dec (False : False : l) = (ValNil, l)
dec (True : True : l) = (ValCons v1 v2, l3)
  where (v1, l2) = dec l
        (v2, l3) = dec l2
dec (False : True : l) = (ValInt n, l2)
  where (n, l2) = decLenInt 0 l
dec (True : False : l) = (ValInt (-n), l2)
  where (n, l2) = decLenInt 0 l

decString = fst . dec . map(\c -> c == '1')


encInt n = (if n >= 0 then [False, True] else [True, False])
  ++ replicate len True
  ++ [False]
  ++ map (testBit absn) (reverse [0 .. 4*len-1])
  where
    absn = abs n
    len = head $ dropWhile (\l -> 16^l <= absn) [0..]

enc (ValInt n) = encInt n
enc (ValNil) = [False, False]
enc (ValCons v1 v2) = [True, True] ++ enc v1 ++ enc v2

encString v = map (\b -> if b then '1' else '0') (enc v)
