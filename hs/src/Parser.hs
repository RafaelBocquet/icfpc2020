module Parser where

import Control.Monad.State
import Token

-- data Expr = EInt Integer
--           | EPair Expr Expr

-- newtype Parser a = Parser { runParser :: [Token] -> Maybe (a, Token) }
--                  deriving (Functor)

-- instance Applicative Parser where
--   pure = return
--   (<*>) = ap

-- instance Monad Parser where
--   return a = Parser $ \s -> Just (a, s)
--   Parser f >>= g = Parser $ \s ->
--     case f s of
--       Nothing     -> Nothing
--       Just(a, s') -> runParser (g a) s'

-- parseInt :: Parser Integer
-- parseInt = _

-- parseExpr :: Parser Expr
-- parseExpr = _
