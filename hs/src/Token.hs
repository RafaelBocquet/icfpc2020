module Token where

import Control.Applicative
import Control.Monad
import Data.Maybe
import Data.Foldable

data Token = TkInt Integer
           | TkVar Integer

           | TkIs

           | TkAp

           | TkSucc
           | TkPred
           | TkAdd
           | TkMul
           | TkFloorDiv
           | TkEq
           | TkLessThan
           | TkNeg
           | TkTwoPow

           | TkSComb
           | TkCComb
           | TkBComb
           | TkIComb

           | TkTrue
           | TkFalse

           | TkEncode
           | TkDecode

           | TkPair
           | TkFst
           | TkSnd

           | TkNil
           | TkCons

           | TkIsNil

           | TkStartList
           | TkSepList
           | TkEndList

           | TkPixel
           | TkDisplay
           | TkDisplayList
           | TkCheckerboard

           | TkIfInt

           | TkInteract
          
           | TkUnknown [[Bool]]
           deriving (Show, Eq, Ord)

decodeInteger :: [[Bool]] -> Maybe Integer
decodeInteger xs
  | (xs !! 0 !! 0) == False && all (\i -> xs !! i !! 0) [1..n-1] && all (\j -> xs !! 0 !! j) [1..m-1]
    = Just (if n == m then val else -val)
  | otherwise = Nothing
  where n = length xs
        m = length (head xs)
        bits =  concat (map tail (tail xs))
        val = foldr' (\b n -> 2*n+(if b then 1 else 0)) 0 bits

decodeVar :: [[Bool]] -> Maybe Token
decodeVar xs
  | n > 3 && m > 3 && all (\i -> xs !! i !! 0) [0..n-1] && all (\i -> xs !! i !! (m-1)) [0..n-1] && all (\i -> xs !! 0 !! i) [0..m-1] && all (\i -> xs !! (n-1) !! i) [0..m-1]
    = TkVar <$> decodeInteger (map (map not) $ map (tail.init) ((tail.init) xs))
  | otherwise = Nothing
  where n    = length xs
        m    = length (head xs)


decodeEq :: [[Bool]] -> Token -> [[Bool]] -> Maybe Token
decodeEq pat tk s
  | pat == s = Just tk
  | otherwise = Nothing

t = True
f = False

baseTokens :: [([[Bool]], Token)]
baseTokens =
  [ ( [ [t,t],
        [t,f] ]
    , TkAp)

  , ( [ [t,t,t],
        [t,f,f],
        [t,t,t] ]
    , TkIs)

  , ( [ [t,t,t],
        [t,f,t],
        [t,f,t] ]
    , TkNeg)

  , ( [ [t,t,t,t],
        [t,t,f,f],
        [t,f,f,t],
        [t,f,t,t] ]
    , TkSucc)

  , ( [ [t,t,t,t],
        [t,t,f,f],
        [t,f,t,f],
        [t,f,t,t] ]
    , TkPred)

  , ( [ [t,t,t,t],
        [t,t,f,t],
        [t,t,f,t],
        [t,t,f,t] ]
    , TkAdd)

  , ( [ [t,t,t,t],
        [t,f,t,f],
        [t,f,t,f],
        [t,f,t,f] ]
    , TkMul)

  , ( [ [t,t,t,t],
        [t,f,f,f],
        [t,t,f,t],
        [t,f,f,f] ]
    , TkFloorDiv)

  , ( [ [t,t,t,t],
        [t,f,f,f],
        [t,f,f,f],
        [t,t,t,t] ]
    , TkEq)

  , ( [ [t,t,t,t],
        [t,f,f,f],
        [t,f,f,t],
        [t,f,t,t] ]
    , TkLessThan)

  , ( [ [t,t,t,t],
        [t,f,t,f],
        [t,t,f,t],
        [t,f,t,f] ]
    , TkEncode)

  , ( [ [t,t,t,t],
        [t,t,f,t],
        [t,f,t,f],
        [t,t,f,t] ]
    , TkDecode)

  , ( [ [t,t,t],
        [t,t,t],
        [t,t,f] ]
    , TkSComb)

  , ( [ [t,t,t],
        [t,f,t],
        [t,t,f] ]
    , TkCComb)

  , ( [ [t,t,t],
        [t,t,f],
        [t,t,f] ]
    , TkBComb)

  , ( [ [t,t,t],
        [t,f,t],
        [t,f,f] ]
    , TkTrue)

  , ( [ [t,t,t],
        [t,f,f],
        [t,f,t] ]
    , TkFalse)

  , ( [ [t,t,t,t,t,t,t],
        [t,f,f,f,f,f,t],
        [t,f,f,t,t,f,t],
        [t,f,t,f,t,f,t],
        [t,f,t,f,f,f,t],
        [t,f,f,f,f,f,t],
        [t,t,t,t,t,t,t] ]
    , TkTwoPow)

  , ( [ [t,t],
        [t,t] ]
    , TkIComb)


  , ( [ [t,t,t,t,t],
        [t,f,t,f,t],
        [t,f,t,f,t],
        [t,f,t,f,t],
        [t,t,t,t,t] ]
    , TkCons)

  , ( [ [t,t,t,t,t],
        [t,f,t,t,t],
        [t,f,t,f,t],
        [t,f,t,f,t],
        [t,t,t,t,t] ]
    , TkFst)

  , ( [ [t,t,t,t,t],
        [t,t,t,f,t],
        [t,f,t,f,t],
        [t,f,t,f,t],
        [t,t,t,t,t] ]
    , TkSnd)

  , ( [ [t,t,t],
        [t,f,t],
        [t,t,t] ]
    , TkNil)

  , ( [ [t,t,t],
        [t,t,t],
        [t,t,t] ]
    , TkIsNil)

  , ( [ [f,f,t],
        [f,t,t],
        [t,t,t],
        [f,t,t],
        [f,f,t] ]
    , TkStartList)

  , ( [ [t,t],
        [t,t],
        [t,t],
        [t,t],
        [t,t] ]
    , TkSepList)

  , ( [ [t,f,f],
        [t,t,f],
        [t,t,t],
        [t,t,f],
        [t,f,f] ]
    , TkEndList)

  , ( [ [t,t,t,t,t,t],
        [t,t,f,f,f,f],
        [t,f,t,f,f,f],
        [t,f,f,t,f,f],
        [t,f,f,f,t,f],
        [t,f,f,f,f,t] ]
    , TkPixel)

  , ( [ [t,t,t,t,t],
        [t,f,f,f,t],
        [t,f,f,f,t],
        [t,f,f,f,t],
        [t,t,t,t,t] ]
    , TkDisplay)

  , ( [ [t,t,t,t,t,t],
        [t,f,t,f,t,f],
        [t,t,f,t,f,t],
        [t,f,t,f,t,f],
        [t,t,f,t,f,t],
        [t,f,t,f,t,f] ]
    , TkCheckerboard)

  , ( [ [t,t,t,t,t],
        [t,f,f,f,f],
        [t,f,t,t,t],
        [t,t,t,f,f],
        [t,f,t,t,t] ]
    , TkIfInt)

  , ( [ [t,t,t,t,t,t],
        [t,f,f,f,f,t],
        [t,f,t,t,f,t],
        [t,f,t,t,f,t],
        [t,f,f,f,f,t],
        [t,t,t,t,t,t] ]
    , TkInteract )
  ]

decodeSymbol :: [[Bool]] -> Token
decodeSymbol s = maybe (TkUnknown s) id $
                 -- Single symbols
                 asum (map (\(pat, tk) -> do guard (pat == s); pure tk) baseTokens)
                 -- Integers
                 <|> (TkInt <$> decodeInteger s)
                 -- Variables
                 <|> decodeVar s
