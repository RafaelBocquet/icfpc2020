module ParseImage where

import Token

import System.Environment (getArgs)
import Control.Monad
import Control.Monad.ST
import Control.Monad.Identity
import Data.List
import Data.Maybe
import Data.Word

import qualified Codec.Picture as P
import qualified Codec.Picture.RGBA8 as P8
import qualified Codec.Picture.Types as P8
import qualified Data.Vector.Mutable as V

decImg img = transpose a
  where w = P.imageWidth img
        h = P.imageHeight img
        a = [[P.pixelAt img i j == P8.PixelRGBA8 255 255 255 255  | j <- [4,8..h-5]] | i <- [4,8..w-5]]

imageLines :: [[Bool]] -> [[[Bool]]]
imageLines [] = []
imageLines (x:xs)
  | any id x     = takeWhile (any id) (x:xs) : imageLines (dropWhile (any id) (x:xs))
  | otherwise = imageLines xs

lineSymbols :: [[Bool]] -> [[[Bool]]]
lineSymbols xs = transpose <$> imageLines (transpose xs)

reduceSymbol :: [[Bool]] -> [[Bool]]
reduceSymbol = reverse . dropWhile (all not) . reverse

parseImage :: IO ()
parseImage = do
  [fname] <- getArgs
  img <- P8.readImageRGBA8 fname
  putStrLn "Image :"
  forM_ (decImg img) $ \line -> do
    forM line $ \b -> do
      putStr (if b then "1" else "0")
    putStrLn ""
  let lines = imageLines (decImg img)
  forM_ (zip [1..] lines) $ \(iline, line) -> do
    putStrLn $ "Line n°" ++ show iline ++ " :"
    forM_ (decodeSymbol . reduceSymbol <$> lineSymbols line) $ \tk -> do
      print tk
    --   forM_ (reduceSymbol sym) $ \sline -> do
    --     forM sline $ \b -> do
    --       putStr (if b then "1" else "0")
    --     putStrLn ""
    --   putStrLn ""
  pure ()
