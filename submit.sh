#!/bin/sh
if [ -z "$(git status --porcelain)" ]; then
  if [[ "$1" =~ ^[a-zA-Z0-9_-]+$ ]]; then
      git branch "submissions/$1"
      git checkout "submissions/$1"
      git commit --allow-empty -m "submission: $1"
      git push origin "submissions/$1"
      git checkout master
      git branch -D "submissions/$1"
  else
      echo -e "\e[31m\e[7mPlease only use characters in [a-zA-Z0-9_-] for the submission branch name.\e[0m"
  fi
else
  echo -e "\e[31m\e[7mThe working directory is not clean:\e[0m"
  git status --porcelain
fi
