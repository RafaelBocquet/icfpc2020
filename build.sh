#!/bin/sh
mkdir -p build/release
cmake -S . -B build/release -DCMAKE_BUILD_TYPE=Release
cmake --build build/release -- client VERBOSE=1
