#!/usr/bin/env bash
data=$(build/release/send '[1 0]' | tr -d '[]')
key1=$(echo "$data" | cut -d' ' -f 3)
key2=$(echo "$data" | cut -d' ' -f 5)
echo "Key1: $key1"
echo "Key2: $key2"

red=$(tput setaf 1)
green=$(tput setaf 2)
default=$(tput sgr0)
build/debug/local_client $key1 2>&1 | sed "s/.*/$red&$default/" &
build/debug/local_client $key2 2>&1 | sed "s/.*/$green&$default/" &

wait
