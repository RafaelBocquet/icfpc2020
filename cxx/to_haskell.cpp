#include "header.hpp"
#include "code_stream.hpp"
#include "expr.hpp"
#include "parse_program.hpp"


enum EXPR_TYPE
{ ET_INT,
  ET_APP,
  ET_NAME,

  ET_NIL,

  ET_SUCC, ET_PRED,
  ET_ADD, ET_MUL, ET_DIV, ET_EQ, ET_LT,
  ET_NEG, ET_2POW,

  ET_PAIR,
  ET_S,
  ET_C,
  ET_B,
  ET_I,

  ET_TRUE, ET_FALSE,

  ET_ISNIL, ET_CAR, ET_CDR
};

struct expr;
using expr_ptr = shared_ptr<expr>;
struct expr {
  EXPR_TYPE type;
  ZZ        value;
  expr_ptr  f, x;
  string    name;
};

static inline ostream& operator<<(ostream& out, expr_ptr const& e) {
  if(!e) { debug("bug"); assert(0); }
  if(e->type == ET_INT) out << e->value;
  else if(e->type == ET_APP) out << "(" << e->f << " " << e->x << ")";
  else if(e->type == ET_NAME) out << e->name;
  else if(e->type == ET_NIL) out << "nil";
  else if(e->type == ET_SUCC) out << "inc";
  else if(e->type == ET_PRED) out << "dec";
  else if(e->type == ET_ADD) out << "add";
  else if(e->type == ET_MUL) out << "mul";
  else if(e->type == ET_DIV) out << "div";
  else if(e->type == ET_EQ) out << "eq";
  else if(e->type == ET_LT) out << "lt";
  else if(e->type == ET_S) out << "s";
  else if(e->type == ET_C) out << "c";
  else if(e->type == ET_B) out << "b";
  else if(e->type == ET_I) out << "i";
  else if(e->type == ET_NEG) out << "neg";
  else if(e->type == ET_2POW) out << "pwr2";
  else if(e->type == ET_TRUE) out << "true";
  else if(e->type == ET_FALSE) out << "false";
  else if(e->type == ET_CAR) out << "car";
  else if(e->type == ET_CDR) out << "cdr";
  else if(e->type == ET_ISNIL) out << "isnil";
  else if(e->type == ET_PAIR) out << "cons";
  return out;
}

static inline expr_ptr expr_int(ZZ const& v) {
  expr e; e.type = ET_INT; e.value = v;
  return make_shared<expr>(e);
}

static inline expr_ptr expr_app(expr_ptr l, expr_ptr r) {
  expr m; m.type = ET_APP; m.f = l; m.x = r;
  return make_shared<expr>(m);
}

static inline expr_ptr expr_name(string atom) {
  expr m; m.type = ET_NAME; m.name = atom;
  return make_shared<expr>(m);
}

static inline expr_ptr expr_primitive(EXPR_TYPE type) {
  expr e; e.type = type; return make_shared<expr>(e);
}

static inline expr_ptr expr_prim_or_name(string s) {
#define TESTPRIM(t, prim) if(s == t) return expr_primitive(prim);
  TESTPRIM("inc", ET_SUCC);
  TESTPRIM("dec", ET_PRED);
  TESTPRIM("add", ET_ADD);
  TESTPRIM("mul", ET_MUL);
  TESTPRIM("div", ET_DIV);
  TESTPRIM("eq", ET_EQ);
  TESTPRIM("lt", ET_LT);
  TESTPRIM("neg", ET_NEG);
  TESTPRIM("s", ET_S);
  TESTPRIM("c", ET_C);
  TESTPRIM("b", ET_B);
  TESTPRIM("i", ET_I);
  TESTPRIM("t", ET_TRUE);
  TESTPRIM("f", ET_FALSE);
  TESTPRIM("pwr2", ET_2POW);
  TESTPRIM("cons", ET_PAIR);
  TESTPRIM("car", ET_CAR);
  TESTPRIM("cdr", ET_CDR);
  TESTPRIM("nil", ET_NIL);
  TESTPRIM("isnil", ET_ISNIL);
  return expr_name(s);
#undef TESTPRIM
}

string clean_name(string const& o);
void to_haskell(ostream& out, expr_ptr const& e);


string clean_name(string const& o) {
  string r;
  for(char c : o) {
    if(c == ':') r.pb('n');
    else r.pb(c);
  }
  return r;
}

void to_haskell(ostream& out, expr_ptr const& e) {
  assert(e);
  switch(e->type) {
    case ET_INT: {
      out << "VInt (" << e->value << ")";
      return;
    }
    case ET_APP: {
      out << '(';
      to_haskell(out, e->f);
      out << ") $$ (";
      to_haskell(out, e->x);
      out << ")";
      return;
    }
    case ET_NIL: {
      out << "VNil";
      return;
    }
    case ET_NAME: {
      out << clean_name(e->name);
      return;
    }
    case ET_SUCC: {
      out << "vsucc";
      return;
    }
    case ET_PRED: {
      out << "vpred";
      return;
    }
    case ET_ADD: {
      out << "vadd";
      return;
    }
    case ET_MUL: {
      out << "vmul";
      return;
    }
    case ET_DIV: {
      out << "vquot";
      return;
    }
    case ET_EQ: {
      out << "veq";
      return;
    }
    case ET_LT: {
      out << "vlt";
      return;
    }
    case ET_NEG: {
      out << "vneg";
      return;
    }
    case ET_2POW: {
      out << "vpwr2";
      return;
    }
    case ET_PAIR: {
      out << "vCons";
      return;
    }
    case ET_S: {
      out << "vS";
      return;
    }
    case ET_C: {
      out << "vC";
      return;
    }
    case ET_B: {
      out << "vB";
      return;
    }
    case ET_I: {
      out << "vI";
      return;
    }
    case ET_TRUE: {
      out << "vtrue";
      return;
    }
    case ET_FALSE: {
      out << "vfalse";
      return;
    }
    case ET_ISNIL: {
      out << "vIsNil";
      return;
    }
    case ET_CAR: {
      out << "vCar";
      return;
    }
    case ET_CDR: {
      out << "vCdr";
      return;
    }
  }
}

int main(int argc, char** argv) {
  assert(argc == 2);
  char* filename = argv[1];

  ifstream in(filename);
  if(!in.good()) { debug(filename); exit(1); }
  string pline;
  while(getline(in, pline)) {
    string name; expr_ptr e;
    tie(name, e) = parse_program_line(pline);
    if(name == "") continue;
    cout << clean_name(name) << " = ";
    to_haskell(cout, e);
    cout << endl;
  }
  return 0;
}
