#pragma once
#include "header.hpp"
#include "code_stream.hpp"

enum EXPR_TYPE
{ ET_INT,
	ET_APP,
	ET_ATOM
};

struct expr;
using expr_ptr = shared_ptr<expr>;
struct expr {
	EXPR_TYPE type;
	ZZ intvalue;
	expr_ptr l, r;
	string atomvalue;
};

static inline ostream& operator<<(ostream& out, expr_ptr const& e) {
	if(!e) { debug("bug"); exit(1); }
	if(e->type == ET_INT) out << e->intvalue;
	else if(e->type == ET_APP) out << "(" << e->l << " " << e->r << ")";
	else if(e->type == ET_ATOM) out << e->atomvalue;
	return out;
}

static inline expr_ptr expr_int(ZZ const& v) {
	expr e; e.type = ET_INT; e.intvalue = v;
	return make_shared<expr>(e);
}

static inline expr_ptr expr_app(expr_ptr l, expr_ptr r) {
	expr m; m.type = ET_APP; m.l = l; m.r = r;
	return make_shared<expr>(m);
}

static inline expr_ptr expr_atom(string atom) {
	expr m; m.type = ET_ATOM; m.atomvalue = atom;
	return make_shared<expr>(m);
}

enum VALUE_TYPE {
	VT_INT,
	VT_NIL, VT_INT1,

	VT_SUCC, VT_PRED,
	VT_ADD, VT_ADD1, VT_MUL, VT_MUL1, VT_FLOORDIV, VT_FLOORDIV1, VT_EQ, VT_EQ1, VT_LT, VT_LT1,
	VT_NEG, VT_2POW,

	VT_PAIR, VT_PAIR1, VT_PAIR2,
	VT_S, VT_S1, VT_S2,
	VT_C, VT_C1, VT_C2,
	VT_B, VT_B1, VT_B2,
	VT_I,

	VT_TRUE, VT_TRUE1, VT_FALSE, VT_FALSE1,

	VT_ISNIL, VT_CAR, VT_CDR,

	VT_LAZY, VT_LAZYAPP, VT_FORWARD
};

struct value;
using value_ptr = shared_ptr<value>;
struct value {
	VALUE_TYPE type;
	ZZ         intvalue;
	value_ptr  val1, val2;
	expr_ptr   e;
};

#define showcase(s) case s: out << #s; break
static inline ostream& operator<<(ostream& out, VALUE_TYPE vt) {
	switch (vt) {
		showcase(VT_INT);
		showcase(VT_INT1);
  	showcase(VT_NIL);
		showcase(VT_SUCC);
		showcase(VT_PRED);
		showcase(VT_ADD);
		showcase(VT_ADD1);
		showcase(VT_MUL);
		showcase(VT_MUL1);
		showcase(VT_FLOORDIV);
		showcase(VT_FLOORDIV1);
		showcase(VT_EQ);
		showcase(VT_EQ1);
		showcase(VT_LT);
		showcase(VT_LT1);
		showcase(VT_NEG);
		showcase(VT_2POW);
		showcase(VT_PAIR);
		showcase(VT_PAIR1);
		showcase(VT_PAIR2);
		showcase(VT_S);
		showcase(VT_S1);
		showcase(VT_S2);
		showcase(VT_C);
		showcase(VT_C1);
		showcase(VT_C2);
		showcase(VT_B);
		showcase(VT_B1);
		showcase(VT_B2);
		showcase(VT_I);
		showcase(VT_TRUE);
		showcase(VT_TRUE1);
		showcase(VT_FALSE);
		showcase(VT_FALSE1);
		showcase(VT_ISNIL);
		showcase(VT_CAR);
		showcase(VT_CDR);
		showcase(VT_LAZY);
		showcase(VT_LAZYAPP);
		showcase(VT_FORWARD);
  	default: debug("bug"); exit(1);
	}
	return out;
}

static inline ostream& operator<<(ostream& out, value_ptr const& v) {
	if(!v) { debug("bug"); exit(1); }
	switch (v->type) {
	  case VT_INT:
			out << "(" << v->type << " " << v->intvalue << ")";
			break;

	  case VT_INT1:
			out << "(" << v->type << " " << v->intvalue << " " << v->val1 << ")";
			break;

	  case VT_NIL: case VT_SUCC: case VT_PRED: case VT_ADD: case VT_MUL: case VT_FLOORDIV: case VT_EQ: case VT_LT: case VT_NEG: case VT_2POW: case VT_PAIR: case VT_S: case VT_C: case VT_B: case VT_I: case VT_TRUE: case VT_FALSE: case VT_ISNIL: case VT_CAR: case VT_CDR:
			out << v->type;
			break;

  	case VT_PAIR1: case VT_S1: case VT_C1: case VT_B1: case VT_TRUE1: case VT_FALSE1: case VT_ADD1: case VT_MUL1: case VT_FLOORDIV1: case VT_EQ1: case VT_LT1:
			out << "(" << v->type << " " << v->val1 << ")";
			break;

	  case VT_PAIR2: case VT_S2: case VT_C2: case VT_B2: case VT_LAZYAPP: 
			out << "(" << v->type << " " << v->val1 << " " << v->val2 << ")";
			break;

	  case VT_LAZY:
		  out << "(" << v->type << " " << v->e << ")";
			break;

  	case VT_FORWARD:
			out << "FORWARD";
			break;

  	default:
			debug("bug"); exit(1);
	}
	return out;
}


static inline value_ptr value_intlike(VALUE_TYPE type, ZZ const& x) {
	value v; v.type = type; v.intvalue = x;
	return make_shared<value>(v);
}

static inline value_ptr value_int1(VALUE_TYPE type, ZZ const& x, value_ptr v1) {
	value v; v.type = type; v.intvalue = x; v.val1 = v1;
	return make_shared<value>(v);
}

static inline value_ptr value_0(VALUE_TYPE type) {
	value v; v.type = type;
	return make_shared<value>(v);
}

static inline value_ptr value_1(VALUE_TYPE type, value_ptr v1) {
	value v; v.type = type; v.val1 = v1;
	return make_shared<value>(v);
}

static inline value_ptr value_2(VALUE_TYPE type, value_ptr v1, value_ptr v2) {
	value v; v.type = type; v.val1 = v1; v.val2 = v2;
	return make_shared<value>(v);
}

static inline value_ptr value_lazy(expr_ptr e) {
	value v; v.type = VT_LAZY; v.e = e;
	return make_shared<value>(v);
}


value_ptr apply(value_ptr f, value_ptr x);
value_ptr eval(expr_ptr e);
message_ptr message_of_value(value_ptr v);
value_ptr value_of_message(message_ptr m);
void init_defs();
void defname(string const& name, expr_ptr e);
value_ptr force_name(string const& name);
value_ptr force_value(value_ptr x);
