#include "header.hpp"
#include "code_stream.hpp"

void run_client(ZZ playerKey, function<message_ptr(message_ptr const&)> post);
