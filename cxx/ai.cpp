#include "ai.hpp"

// extern in ai.hpp ----
int cur_turn;
vector<ship> ships;
game_static gw;
// ---------------------

// random
static struct {
  uint32_t x = 1, y = 2, z = 3, w = 4;

  inline void reset(uint32_t seed) {
    x = seed; y = seed+1; z = seed+2; w = seed+3;
  }

  inline uint32_t xorshift128() {
    uint32_t t = x;
    t ^= t << 11;
    t ^= t >> 8;
    x = y; y = z; z = w;
    w ^= w >> 19;
    w ^= t;
    return w;
  }
} xorshift128_s;

void random_reset(int64_t seed = chrono::steady_clock::now().time_since_epoch().count()) { xorshift128_s.reset(seed); }

int64_t random(int64_t r) {
  return xorshift128_s.xorshift128()%r;
}

int64_t random(int64_t l, int64_t r) {
  return l + random(r-l+1);
}

double randomDouble() {
  return (double)random(2147483648) / 2147483648.0;
}

// Config

const bool use_double_moves   = false;
const bool use_atk_laser      = true;
const bool use_atk_spawn      = true;
const bool use_atk_double_dir = true;

const int atk_base_laser   = 48;
const int atk_base_cooling = 12;
const int atk_base_clones  = 24;

const int atk_base_health  = 512 - 4 * atk_base_laser - 12 * atk_base_cooling - 2 * atk_base_clones;
static_assert(atk_base_health > 1);

// TODO: defense laser to kill low cooling enemies ?
const int def_base_laser   = 0;
const int def_base_cooling = 16;
const int def_base_clones  = 32;

const int def_base_health  = 448 - 4 * def_base_laser - 12 * def_base_cooling - 2 * def_base_clones;
static_assert(def_base_health > 1);


/// ....

vec2 move_dir(int move_ix) {
  int dx = (move_ix/5) - 2;
  int dy = (move_ix%5) - 2;
  return vec2{dx, dy};
}

int norm_infty(vec2 p) {
  return max(abs(p.x), abs(p.y));
}

int move_cost(int x) {
  return norm_infty(move_dir(x));
}

int move_ix(int x, int y) {
  return (x+2)*5 + (y+2);
}

int move_ix(vec2 m) {
  return move_ix(m.x, m.y);
}

vec2 planet_accel_at(int x, int y) {
  int dx = 0, dy = 0;
  if(abs(x) >= abs(y)) {
    if(x > 0) dx -= 1;
    if(x < 0) dx += 1;
  }
  if(abs(y) >= abs(x)) {
    if(y > 0) dy -= 1;
    if(y < 0) dy += 1;
  }
  return vec2{dx,dy};
}

vec2 planet_accel_at(vec2 pos) {
  return planet_accel_at(pos.x, pos.y);
}

bool in_bounds(vec2 pos) {
  return max(abs(pos.x), abs(pos.y)) > gw.planet_radius && max(abs(pos.x), abs(pos.y)) < gw.world_radius;
}

int ship_total(ship_stats stats) {
	return stats.health + stats.attack + stats.cooling + stats.clones;
}

int explosion_strength(int total) {
	return (int)(128. * sqrt(log2(total+1)));
}

int ship::score(){
  return stats.health + 4 * stats.attack + 12 * stats.cooling + 2 * stats.clones;
}

double angle(vec2 p) {
  int x = p.x, y = p.y;
  if(abs(x) >= abs(y)) {
    if(x > 0) {
      return 1.0 - (double)y/(double)x;
    } else {
      return 5.0 - (double)y/(double)x;
    }
  } else {
    if(y > 0) {
      return 7.0 + (double)x/(double)y;
    } else {
      return 3.0 + (double)x/(double)y;
    }
  }
}

double rotation_at(vec2 p, vec2 s) {
  double r0 = angle(p);
  double r1 = angle(p+s);
  double r = r1-r0;
  if(r < -4.0) r += 8.0;
  if(r > 4.0) r -= 8.0;
  return r;
}

bool atk_fire_angle(vec2 p, vec2 s) {
	vec2 u = p - s;
	int x = abs(u.x), y = abs(u.y);
	if (y > x) swap(x, y);
	int z = abs(x - 2 * y);
	return 4 * z >= x;
	//return true;
}

bool good_fire_angle(vec2 p, vec2 s) {
	vec2 u = p - s;
	int x = abs(u.x), y = abs(u.y);
	if (y > x) swap(x, y);
	int z = abs(x - 2 * y);
	return 2 * z >= x;
	//return true;
}

struct traj{
  vector<vec2> pos;
  vector<vec2> spd;
  int  turn;
  int  until;

  traj() {
    pos.resize(gw.max_turn);
    spd.resize(gw.max_turn);
  }

  double total_rotation; // total rotation
  double height;         // average distance to center

  void compute(int turn_, vec2 p, vec2 s){
    turn = turn_;
    until = gw.max_turn-1; // infty
    int sum_distance = 0;
    total_rotation = 0.0;
    FORU(i, turn, gw.max_turn-1) {
      pos[i] = p; spd[i] = s;
      sum_distance += norm_infty(p);
      total_rotation += rotation_at(p, s);
      s += planet_accel_at(p);
      p += s;
      if(!in_bounds(p)) {
        until = i;
        break;
      }
    }

    height = (double)sum_distance/(double)(until-turn+1);
  }

  bool expected_to_crash() const {
    return until <= turn + 32 && until < gw.max_turn;
  }

  bool is_stable() const {
    return until >= gw.max_turn - 1;
  }

  int dist_point(vec2 to) {
    int d = 1'000'000;
    FORU(i, turn, until) {
      d = min(d, to.dist(pos[i]));
    }
    return d;
  }

  bool is_ccw() { return total_rotation > 0.5; }
  bool is_cw() { return total_rotation < -0.5; }

  int dist_traj(traj const& to) {
    assert(turn >= to.turn);
    int d = 1'000'000;
    FORU(i, turn, until) if(i <= to.until) {
      d = min(d, pos[i].dist(to.pos[i]));
    }
    return d;
  }

  int dist_traj_with_speed(traj const& to) {
    assert(turn >= to.turn);
    int d = 1'000'000;
    FORU(i, turn, until) if(i <= to.until) {
      d = min(d, pos[i].dist(to.pos[i]) + spd[i].dist(to.spd[i]));
    }
    return d;
  }
};

tuple<vec2, vec2> apply_move(vec2 pos, vec2 spd, vec2 mv) {
  vec2 s = spd + mv + planet_accel_at(pos);
  vec2 p = pos + s;
  return mt(p, s);
}

tuple<vec2, vec2> apply_move(vec2 pos, vec2 spd, int mv) {
  return apply_move(pos, spd, move_dir(mv));
}

int beam_avoid_danger(vector<int> const& en_ships, int turn, vec2 pos, vec2 spd, int available_health, bool enemy_dangerous) {
  int ne = en_ships.size();
  vector<traj> en_trajs(ne);
  FOR(i, ne) {
    auto const& ship = ships[en_ships[i]];
    en_trajs[i].compute(turn, ship.pos, ship.spd);
  }

  auto distance_at = [&](int turn, vec2 pos, vec2 spd) {
    int d = 16;
    FOR(i, ne) if(turn <= en_trajs[i].until) {
      d = min(d, en_trajs[i].pos[turn].dist(pos));
    }
    return d;
  };

  const int BEAM_DEPTH       = 128;
  const int BEAM_WIDTH       = 25;
  const int beam_length      = 32;
  struct beam_state {
    int  turn;
    vec2 pos;
    vec2 spd;
    int  min_distance;
    int  total_cost;

    int first_move;

    tuple<int, int> score() const { return mt(-min_distance, total_cost); } // to minimize
  };
  beam_state BEAM_A[BEAM_DEPTH];
  int na = 1; BEAM_A[0] = {turn, pos, spd, distance_at(turn, pos, spd), 0, -1};
  beam_state BEAM_B[BEAM_DEPTH * BEAM_WIDTH];

  // score = (min seen distance to enemies, total fuel cost) ???

  //TODO : detecter les tirs lasers

  while(1) {
    int nb = 0;
    FOR(ia, na) {
      auto const& sa = BEAM_A[ia];
      if(sa.turn == gw.max_turn-1 || sa.turn == turn + beam_length) continue;
      FOR(mv, 25) {
        int co = move_cost(mv);
        // if(co == 2) continue;
        if (sa.first_move == -1 && co == 0 && enemy_dangerous) continue;
				vec2 p, s; tie(p, s) = apply_move(sa.pos, sa.spd, mv);
				if (sa.first_move == -1) {
					traj t; t.compute(sa.turn + 1, p, s);
					if (!t.is_stable()) continue;
				}
        if(!in_bounds(p)) continue;
        if(sa.total_cost + co > available_health/2) continue;
        int di = distance_at(sa.turn+1, p, s);
        BEAM_B[nb] = {sa.turn+1, p, s,
                      min(sa.min_distance, di),
                      sa.total_cost + co,
                      (sa.first_move==-1 ? mv : sa.first_move)};
        nb++;
      }
    }
    pdqsort(BEAM_B, BEAM_B+nb, [&](beam_state const& a, beam_state const& b) { return a.score() < b.score(); });
    if(nb == 0) {
      return BEAM_A[0].first_move;
    }
    na = 0;
    set<tuple<vec2, vec2>> SEEN;
    FOR(i, nb) if(na < BEAM_DEPTH) {
      auto const& s = BEAM_B[i];
      if(SEEN.count(mt(s.pos, s.spd))) continue;
      SEEN.insert(mt(s.pos, s.spd));
      BEAM_A[na] = s;
      na++;
    }
  }
}

int reach_orbit_cw(int turn, vec2 pos, vec2 spd) {
  int bestMove = -1;
  int bestStable = 0;
  double bestRotation = -1e9;
  int bestCost = 0;
  FOR(mv, 25) {
    // if(!use_double_moves && move_cost(mv) == 2) continue;
    vec2 p, s; tie(p, s) = apply_move(pos, spd, mv);
    if(!in_bounds(p)) continue;
    traj t; t.compute(turn+1, p, s);
    int stable = t.is_stable()?1:0;
    int co = move_cost(mv);
    if(mt(stable, t.total_rotation, -co) > mt(bestStable, bestRotation, -bestCost)) {
      bestMove = mv;
      bestStable = stable;
      bestRotation = t.total_rotation;
      bestCost = co;
    }
  }
  return bestMove;
}

int reach_orbit_ccw(int turn, vec2 pos, vec2 spd) {
  int bestMove = -1;
  int bestStable = 0;
  double bestRotation = 1e9;
  int bestCost = 0;
  FOR(mv, 25) {
    // if(!use_double_moves && move_cost(mv) == 2) continue;
    vec2 p, s; tie(p, s) = apply_move(pos, spd, mv);
    if(!in_bounds(p)) continue;
    traj t; t.compute(turn+1, p, s);
    int stable = t.is_stable()?1:0;
    int co = move_cost(mv);
    if(mt(stable, -t.total_rotation, -co) > mt(bestStable, -bestRotation, -bestCost)) {
      bestMove = mv;
      bestStable = stable;
      bestRotation = t.total_rotation;
      bestCost = co;
    }
  }
  return bestMove;
}

int higher_orbit(int turn, vec2 pos, vec2 spd) {
  int bestMove = -1;
  int bestStable = 0;
  double bestHeight = -1e9;
  int bestCost = 0;
  FOR(mv, 25) {
    // if(!use_double_moves && move_cost(mv) == 2) continue;
    vec2 p, s; tie(p, s) = apply_move(pos, spd, mv);
    if(!in_bounds(p)) continue;
    traj t; t.compute(turn+1, p, s);
    int stable = t.is_stable()?1:0;
    int co = move_cost(mv);
    if(mt(stable, t.height, -co) > mt(bestStable, bestHeight, -bestCost)) {
      bestMove = mv;
      bestStable = stable;
      bestHeight = t.height;
      bestCost = co;
    }
  }
  return bestMove;
}

int lower_orbit(int turn, vec2 pos, vec2 spd) {
  int bestMove = -1;
  int bestStable = 0;
  double bestHeight = 1e9;
  int bestCost = 0;
  FOR(mv, 25) {
    // if(!use_double_moves && move_cost(mv) == 2) continue;
    vec2 p, s; tie(p, s) = apply_move(pos, spd, mv);
    if(!in_bounds(p)) continue;
    traj t; t.compute(turn+1, p, s);
    int stable = t.is_stable()?1:0;
    int co = move_cost(mv);
    if(mt(stable, -t.height, -co) > mt(bestStable, -bestHeight, -bestCost)) {
      bestMove = mv;
      bestStable = stable;
      bestHeight = t.height;
      bestCost = co;
    }
  }
  return bestMove;
}

int reach_orbit(vec2 pos, vec2 spd) {
	cout << "REACH ORBIT" << endl;
  if (abs(pos.x) >= abs(pos.y)) {
    return move_ix((pos.x >= 0 ? 1 : -1), (spd.y >= 0 ? 1 : -1));
  } else {
    return move_ix((spd.x >= 0 ? 1 : -1), (pos.y >= 0 ? 1 : -1));
  }
}

void print_ships() {
  cout << "Ships: " << endl;
  for(auto const& ship : ships) {
    cout << "ship" << endl;
    cout << "  id = " << ship.id << endl;
    cout << "  owner = " << ship.owner << endl;
    cout << "  pos = " << ship.pos << endl;
    cout << "  spd = " << ship.spd << endl;
    cout << "  health = " << ship.stats.health << endl;
    cout << "  attack = " << ship.stats.attack << endl;
    cout << "  cooling = " << ship.stats.cooling << endl;
    cout << "  clones = " << ship.stats.clones << endl;
    cout << "  heat = " << ship.heat << endl;
    cout << "  heat_cap = " << ship.heat_cap << endl;
    cout << "  move_speed = " << ship.move_speed << endl;
  }
}

// AI

enum SHIP_ROLE {
  ATK_MAIN,

  ATK_MAIN_CW,     // rotate in CW
  ATK_MAIN_CCW,    // rotate in CCW
  ATK_EXPLODER,    // stays in orbit and explodes
  ATK_MISSILE,     // stays in orbit and explodes

  DEF_MAIN,

  DEF_DODGER_CW,      // dodges attackers
  DEF_DODGER_CCW,      // dodges attackers
  DEF_SPAWNER,     // moves to high orbit and spawns small ships
  DEF_DUMMY,
};
map<int, SHIP_ROLE> ship_roles;

ship_stats start_ai() {
  random_reset();

  if(gw.defending) {
    ship_roles[0] = ship_roles[1] = DEF_MAIN;
    int h = gw.max_cost - 4 * def_base_laser - 12 * def_base_cooling - 2 * def_base_clones;
    return ship_stats { h, def_base_laser, def_base_cooling, def_base_clones };
  }else{
    ship_roles[0] = ship_roles[1] = ATK_MAIN;
    int h = gw.max_cost - 4 * atk_base_laser - 12 * atk_base_cooling - 2 * atk_base_clones;
    return ship_stats { h, atk_base_laser, atk_base_cooling, atk_base_clones };
  }
}

void move_ship(vector<command> &R, ship& ship, vec2 mv) {
  R.eb(cmd_move(ship.id, mv));
  tie(ship.new_pos, ship.new_spd) = apply_move(ship.pos, ship.spd, mv);
}

bool stable_move(ship& ship, int mv) {
  vec2 new_pos, new_spd;
  tie(new_pos, new_spd) = apply_move(ship.pos, ship.spd, mv);
  traj t; t.compute(cur_turn + 1, new_pos, new_spd);
  return t.is_stable();
}

void move_ship(vector<command> &R, ship& ship, int mv) {
  move_ship(R, ship, move_dir(mv));
}

int explosion_min_damage(const ship& my_ship, const ship& en_ship, int mv) {
  if (move_cost(mv) > my_ship.stats.health) return false;
  vec2 new_pos, new_spd;
  tie(new_pos, new_spd) = apply_move(my_ship.pos, my_ship.spd, mv);
  int d = new_pos.dist(en_ship.new_pos);
  int strength = explosion_strength(ship_total(my_ship.stats) - move_cost(mv));
  int damage = max(0, strength - 32 * (d + en_ship.move_speed));
  return damage;
}

int explode_and_kill(vector<int> const& en_ships, const ship& ship,
                     function<bool(vector<int> const& killedShips, int totalDamage)> f) {
  int bestKilled = 0, bestDamage = 0; int bestmove = -1;
  FOR (mv, 25) if(move_cost(mv) <= ship.stats.health) {
    int nkilled = 0;
    int totalDamage = 0;
    vector<int> killedShips;
    for (int j : en_ships) {
      int damage = explosion_min_damage(ship, ships[j], mv);
      if (damage > 0) totalDamage += min(damage, ship_total(ships[j].stats));
      if (damage >= ship_total(ships[j].stats)) {
        nkilled++;
        killedShips.eb(j);
      }
    }
    if(mt(nkilled, totalDamage) > mt(bestKilled, bestDamage) && f(killedShips, totalDamage)) {
      bestKilled = nkilled;
      bestDamage = totalDamage;
      bestmove = mv;
    }
  }
  return bestmove;
}

bool run_explode_and_kill(vector<command> &R, vector<int> const& en_ships, ship& ship,
                          function<bool(vector<int> const& killedShips, int totalDamage)> f) {
  int mv = explode_and_kill(en_ships, ship, f);
  if (mv == -1) return false;
  move_ship(R, ship, mv);
  R.eb(cmd_explode(ship.id));
  return true;
}

map<int, tuple<vector<int>, vec2, int, int>> missiles;
map<tuple<int, vec2, vec2>, vector<tuple<vector<int>, vec2, int, int>>> missile_stack;
vector<bool> targeted;

int explosion_at_min_damage(vec2 pos, int explosion_size, vec2 enPos) {
  int d = pos.dist(enPos);
  int strength = explosion_strength(explosion_size);
  int damage = max(0, strength - 32 * d);
  return damage;
}

void upd_missiles(vector<int> const& my_ships, vector<int> const& en_ships) {
  // set<int> all_ships(all(my_ships));
  // remove the dead missiles
  // for(auto it = begin(missiles); it != end(missiles);) {
  //   if(!all_ships.count(it->first)) {
  //     missiles.erase(it++);
  //   }else{
  //     ++it;
  //   }
  // }
  // TODO: mark the enemies that are expected to die / are sure to die from missiles

  vector<traj> en_traj(en_ships.size());
  FOR (i, en_ships.size()) {
    en_traj[i].compute(cur_turn, ships[en_ships[i]].pos, ships[en_ships[i]].spd);
  }

  targeted.assign(en_ships.size(), false);
  for (int iship : my_ships) {
    if (ship_roles[iship] != ATK_MISSILE) continue;
    auto const& ship = ships[iship];

    vector<int> moves;
    int until, fuel_left;
    vec2 p;
    tie(moves, p, fuel_left, until) = missiles[ship.id];
    int explosion_strength = 1+fuel_left;

    FOR (j, en_ships.size()) {
      // ship const& en_ship = ships[en_ships[j]];
      int damage = explosion_at_min_damage(p, explosion_strength, en_traj[j].pos[until+1]);
      if (damage > 0) targeted[j] = true;
    }
  }
}


void run_fire_missile(vector<command>& R, int base_turn, vector<int> const& my_ships, vector<int> const& en_ships, ship& ship, int max_fuel) {
  int ne = en_ships.size();
  vector<traj> en_trajs(ne);
  FOR(i, ne) {
    auto const& ship = ships[en_ships[i]];
    en_trajs[i].compute(base_turn, ship.pos, ship.spd);
  }
 
  int nm = my_ships.size();
  vector<traj> my_trajs(nm);
  FOR(i, nm) {
    auto const& ship = ships[my_ships[i]];
    my_trajs[i].compute(base_turn, ship.pos, ship.spd);
  }

  const int MAX_MISSILE_TIME = 16;
  bool found_missile = 0;
  tuple<vector<int>, vec2, int, int> best_missile;
  int bestKill = 0, bestDamage = 0;

  // set<tuple<int, vec2, vec2, int>> SEEN;
  vector<int> cur_missile(gw.max_turn);
  function<void(int, vec2, vec2, int)> backtrack =
    [&nm, &my_ships, &my_trajs, &found_missile, &best_missile, &en_ships, &en_trajs, &base_turn, &cur_missile, &backtrack, &ne, &bestKill, &bestDamage](int turn, vec2 pos, vec2 spd, int fuel_left) {
     
      if(turn >= gw.max_turn) return;
      if(turn > base_turn + MAX_MISSILE_TIME) return;
      // if(SEEN.count(mt(turn, pos, spd, fuel_left))) return;
      // SEEN.insert(mt(turn, pos, spd, fuel_left));

      // try to explode here
      if(turn > base_turn+1){
        int nKill = 0;
        int totalDamage = 0;
        vec2 p; tie(p, ignore) = apply_move(pos, spd, vec2{0,0});
        bool friendlyFire = false;
        FOR(i, nm) if(turn <= my_trajs[i].until) {
          int dam = explosion_at_min_damage(p, 1+fuel_left, my_trajs[i].pos[turn+1]);
          if(dam) {
            friendlyFire = true;
            break;
          }
        }
        FOR(i, ne) if((ships[en_ships[i]].stats.health <= 7 || cur_turn >= gw.max_turn/2) && !targeted[i] && turn <= en_trajs[i].until) {
          int dam = explosion_at_min_damage(p, 1+fuel_left, en_trajs[i].pos[turn+1]);
          int tot = ship_total(ships[en_ships[i]].stats);
          if(dam >= tot) {
            dam = tot;
            nKill += 1;
          }
          totalDamage += dam;
        }
        if(!friendlyFire && (nKill || totalDamage >= 32) && mt(nKill, totalDamage) > mt(bestKill, bestDamage)) {
          found_missile = 1;
          bestKill = nKill;
          bestDamage = totalDamage;
          best_missile = mt(cur_missile, p, fuel_left, turn);
        }
      }

      // try to move
      FOR(m, 25) if(move_cost(m) <= fuel_left) {
        vec2 p, s; tie(p, s) = apply_move(pos, spd, m);
        if(!in_bounds(p)) continue;
        if(move_cost(m) == 0 && turn == base_turn+1) continue;
        cur_missile[turn] = m;
        backtrack(turn+1, p, s, fuel_left - move_cost(m));
        cur_missile[turn] = 0;
      }
    };
  backtrack(base_turn+1, ship.new_pos, ship.new_spd, max_fuel);

  debug(bestKill, bestDamage);
// map<tuple<int, vec2, vec2>, vector<tuple<vector<int>, int>>> missile_stack;
  if(found_missile) {
    R.eb(cmd_split(ship.id, { max_fuel, 0, 0, 1 }));
    debug("MISSILE", mt(base_turn+1, ship.new_pos, ship.new_spd));
    missile_stack[mt(base_turn+1, ship.new_pos, ship.new_spd)].eb(best_missile);
  }
}

void main_atk(vector<command> &R, vector<int> const& my_ships, vector<int> const& en_ships, ship& ship) {
  // Just split into CW and CCW
  move_ship(R, ship, -planet_accel_at(ship.pos));
  if(use_atk_double_dir && ship.stats.clones > 1) {
    R.eb(cmd_split(ship.id, { ship.stats.health/2, 0, min(ship.stats.cooling/4, 2), ship.stats.clones/2 }));
  }
  ship_roles[ship.id] = ATK_MAIN_CW;
}

void main_atk_common(vector<command> &R, vector<int> const& my_ships, vector<int> const& en_ships, ship& ship,
                     bool is_ccw) {
  if(run_explode_and_kill(R, en_ships, ship, [&](vector<int> const& killedShips, int totalDamage){
    return killedShips.size() == en_ships.size();
  })) return;

  // Compute things
  traj t; t.compute(cur_turn, ship.pos, ship.spd);

  bool shared_orbit = false;
  for(int iship : my_ships) {
    auto const& ship2 = ships[iship];
    if(ship2.id == ship.id) continue;
    if(mt(ship.new_pos, ship.new_spd) == mt(ship2.new_pos, ship2.new_spd)) {
      shared_orbit = true;
    }
  }


  // Try to get in orbit
  if(!t.is_stable()) {
    if(is_ccw) {
      int mv = reach_orbit_ccw(cur_turn, ship.pos, ship.spd);
      if(mv != -1) move_ship(R, ship, mv);
      goto main_atk_end_move;
    }else{
       int mv = reach_orbit_cw(cur_turn, ship.pos, ship.spd);
      if(mv != -1) move_ship(R, ship, mv);
      goto main_atk_end_move;
    }
  }

  // Try to move to enemy
  if((shared_orbit || cur_turn%6 == 0) && (ship.stats.health > 20)) {
    double sheight = 0;
    int    cnt = 0;
    for(int i : en_ships) {
      auto const& eship = ships[i];
      traj t2; t2.compute(cur_turn, eship.pos, eship.spd);
      sheight += t2.height;
      cnt++;
    }

    if(sheight/(double)cnt > t.height) {
      { int mv = higher_orbit(cur_turn, ship.pos, ship.spd);
        if(mv != -1) move_ship(R, ship, mv);
        goto main_atk_end_move;
      }
    }else{
      { int mv = lower_orbit(cur_turn, ship.pos, ship.spd);
        if(mv != -1) move_ship(R, ship, mv);
        goto main_atk_end_move;
      }
    }
  }

  // { int mv = beam_follow_enemy(en_ships, cur_turn, ship.pos, ship.spd);
  //   if(mv != -1) move_ship(R, ship, mv);
  //   goto main_atk_end_move;
  // }
main_atk_end_move:;

  // Attack
  if(use_atk_laser) { // do not spawn too fast
    if (ship.heat <= ship.stats.cooling && ship.stats.attack) {
      auto en_ships_sorted = en_ships;
      pdqsort(all(en_ships_sorted), [&](int i, int j) {
        return mt(ships[i].stats.cooling, -ships[i].heat) < mt(ships[j].stats.cooling, -ships[j].heat);
      });
      for(int jship : en_ships_sorted) {
        auto const& ship2 = ships[jship];
        int d = ship.new_pos.dist(ship2.new_pos);
        if (3 <= d && d <= 1.5 * ship.stats.attack && atk_fire_angle(ship.new_pos, ship2.new_pos)) {
          R.eb(cmd_fire(ship.id, ship2.new_pos, ship.stats.attack));
          break;
        }
      }
    }
  }

  if(use_atk_spawn && ship.stats.clones > 1) {
    // test all possible missiles

    run_fire_missile(R, cur_turn, my_ships, en_ships, ship, min(3, ship.stats.health/2));

    // if(t.is_stable() && !shared_orbit && ship.stats.health > 8 && ship.stats.clones > 1) {
    //   R.eb(cmd_split(ship.id, { 4, 0, 0, 1 }));
    // }
  }
}

void main_atk_exploder(vector<command> &R, vector<int> const& my_ships, vector<int> const& en_ships, ship& ship) {
 if(run_explode_and_kill(R, en_ships, ship, [&](vector<int> const& killedShips, int totalDamage){
   return totalDamage >= 64;
  })) return;
}

void main_atk_missile(vector<command> &R, vector<int> const& my_ships, vector<int> const& en_ships, ship& ship) {


  vector<int> moves;
  vec2 pos;
  int fuel_left;
  int until;
  tie(moves, pos, fuel_left, until) = missiles[ship.id];

  if(cur_turn >= until) {
    debug("EXPLODE MISSILE", pos, ship.pos);
    R.eb(cmd_explode(ship.id));
  } else{
    move_ship(R, ship, moves[cur_turn]);
  }
}

void main_def(vector<command> &R, vector<int> const& my_ships, vector<int> const& en_ships, ship& ship) {
 move_ship(R, ship, -planet_accel_at(ship.pos));
  if(ship.stats.clones > 1) {
    R.eb(cmd_split(ship.id, { ship.stats.health/3, 0, ship.stats.cooling/2, ship.stats.clones/2 }));
  }
  ship_roles[ship.id] = DEF_DODGER_CCW;
}

void spawner_def(vector<command> &R, vector<int> const& my_ships, vector<int> const& en_ships, ship& ship) {
  // Compute things
  traj t; t.compute(cur_turn + 1, ship.new_pos, ship.new_spd);

  // Try to get in orbit
  if(!t.is_stable()) {
    int mv = reach_orbit_ccw(cur_turn, ship.pos, ship.spd);
    if(mv != -1) move_ship(R, ship, mv);
    goto def_spawner_end_move;
  }

  { int mv = higher_orbit(cur_turn, ship.pos, ship.spd);
    if(mv != -1) move_ship(R, ship, mv);
    goto def_spawner_end_move;
  }

def_spawner_end_move:;

  if(t.is_stable() && cur_turn%5 == 0 && ship.stats.clones > 1) {
    R.eb(cmd_split(ship.id, { 0, 0, 0, 1 }));
  }
}

void dodger_def(vector<command> &R, vector<int> const& my_ships, vector<int> const& en_ships, ship& ship, bool is_ccw) {
  //TODO: take traj that avoid enemies as long as possible

  if(my_ships.size() > 1) {
		// Check for far away ship
		bool ok = false;
		for (int jship : my_ships) {
			if (ships[jship].new_pos.dist(ship.new_pos) >= 12) ok = true;
		}
    if(ok && run_explode_and_kill(R, en_ships, ship, [&](vector<int> const& killedShips, int totalDamage){
			return killedShips.size() == en_ships.size();
		})) return;
  }

  // Compute things
  traj t; t.compute(cur_turn + 1, ship.new_pos, ship.new_spd);

  bool shared_orbit = false;
  for(int iship : my_ships) {
    auto const& ship2 = ships[iship];
    if(ship2.id == ship.id) continue;
    if(mt(ship.new_pos, ship.new_spd) == mt(ship2.new_pos, ship2.new_spd)) {
      shared_orbit = true;
    }
  }

  // Try to get in orbit
  if(!t.is_stable()) {
    if(is_ccw) {
      int mv = reach_orbit_ccw(cur_turn, ship.pos, ship.spd);
      if(mv != -1) move_ship(R, ship, mv);
      goto def_dodger_end_move;
    }else{
      int mv = reach_orbit_cw(cur_turn, ship.pos, ship.spd);
      if(mv != -1) move_ship(R, ship, mv);
      goto def_dodger_end_move;
    }
  } else {
	}

  // Otherwise try to avoid enemies and laser positions
  {
		bool enemy_dangerous = false;
		for (int jship : en_ships) {
			const auto &en_ship = ships[jship];
			int attack = min(en_ship.stats.attack, en_ship.heat_cap + en_ship.stats.cooling - en_ship.heat);
			int dist = en_ship.new_pos.dist(ship.new_pos);
			if (2 * attack >= dist && (dist <= 20 || good_fire_angle(en_ship.new_pos, ship.new_pos))) enemy_dangerous = true;
		}

		int mv = beam_avoid_danger(en_ships, cur_turn, ship.pos, ship.spd, ship.stats.health, enemy_dangerous);
    if(mv != -1) move_ship(R, ship, mv);
    goto def_dodger_end_move;
  }

def_dodger_end_move:;

  if(t.is_stable() && !shared_orbit && cur_turn%5 == 0 && ship.stats.clones > 1) {
    R.eb(cmd_split(ship.id, { 0, 0, 0, 1 }));
  }
}

void dummy_def(vector <command> &R, vector<int> const& my_ships, vector<int> const& en_ships, ship& ship) {
  if(my_ships.size() > 1) {
    if(run_explode_and_kill(R, en_ships, ship, [&](vector<int> const& killedShips, int totalDamage){
      return totalDamage >= 16;
    })) return;
  }
}

SHIP_ROLE assign_role(ship const& s) {
  if(gw.defending) {
    if(cur_turn <= 2) {
      // return DEF_SPAWNER;
      return DEF_DODGER_CW;
    }else{
      return DEF_DUMMY;
    }
    // if (s.stats.health == 0 && s.stats.attack == 0 && s.stats.cooling == 0 && s.stats.clones == 1) {
    //   return DUMMY_DEF;
    // }
    debug("BUG BUG BUG"); exit(1);
  } else {
    if(cur_turn <= 2) {
      return ATK_MAIN_CCW;
    }else{
      if(missile_stack[mt(cur_turn, s.pos, s.spd)].empty()) {
        debug("BUG BUG BUG");
        debug("BUG BUG BUG");
        debug("BUG BUG BUG");
        exit(1);
      }
      missiles[s.id] = missile_stack[mt(cur_turn, s.pos, s.spd)].back();
      missile_stack[mt(cur_turn, s.pos, s.spd)].pop_back();
      return ATK_MISSILE;
    }
    // if(s.stats.clones > 1)
    debug("BUG BUG BUG"); exit(1);
  }
}

vector<command> run_ai(){
  vector<command> R;

  vector<int> my_ships;
  vector<int> en_ships;

  FOR(iship, ships.size()) {
    auto const& ship = ships[iship];
    if(ship.owner == gw.defending) {
      my_ships.eb(iship);
    }else{
      en_ships.eb(iship);
    }
  }
  if(en_ships.empty()) return {};

  // default ship newpos, newspd
  FOR(iship, ships.size()) {
    auto& ship = ships[iship];
    tie(ship.new_pos, ship.new_spd) = apply_move(ship.pos, ship.spd, vec2{0,0});
  }

  sort(all(my_ships), [&](int i, int j){
    return ships[i].score() > ships[j].score();
  });

  sort(all(en_ships), [&](int i, int j){
    return ships[i].score() > ships[j].score();
  });

  for(int iship : my_ships) {
    auto& ship1 = ships[iship];
    if(!ship_roles.count(ship1.id)) ship_roles[ship1.id] = assign_role(ship1);
  }

  upd_missiles(my_ships, en_ships);

  for(int iship : my_ships) {
    auto& ship1 = ships[iship];
    switch(ship_roles[ship1.id]) {
      case ATK_MAIN:
        main_atk(R, my_ships, en_ships, ship1);
        break;
      case ATK_MAIN_CCW:
        main_atk_common(R, my_ships, en_ships, ship1, 1);
        break;
      case ATK_MAIN_CW:
        main_atk_common(R, my_ships, en_ships, ship1, 0);
        break;
      case ATK_EXPLODER:
        main_atk_exploder(R, my_ships, en_ships, ship1);
        break;
      case ATK_MISSILE:
        main_atk_missile(R, my_ships, en_ships, ship1);
        break;
      case DEF_MAIN:
        main_def(R, my_ships, en_ships, ship1);
        break;
      case DEF_SPAWNER:
        spawner_def(R, my_ships, en_ships, ship1);
        break;
      case DEF_DODGER_CCW:
        dodger_def(R, my_ships, en_ships, ship1, 1);
        break;
      case DEF_DODGER_CW:
        dodger_def(R, my_ships, en_ships, ship1, 0);
        break;
		  case DEF_DUMMY:
				dummy_def(R, my_ships, en_ships, ship1);
				break;
		  default:
				debug("no role for ship"); exit(1);
    }
  }

  return R;
}
