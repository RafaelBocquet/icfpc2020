#include "code_stream.hpp"

void encode_int(ZZ a, vector<bool>& r) {
  if(a<0) { debug("bug"); exit(1); }
  int sz = 0;
  while(a >= (ZZ(1)<<(4*sz))) sz++;
  FOR(i, sz) r.pb(true);
  r.pb(false);
  vector<bool> c;
  FOR(i, 4*sz) { c.pb((a%2)==1); a /= ZZ(2); }
  FOR(i, 4*sz) r.pb(c[4*sz-1-i]);
}

void encode_impl(message_ptr const& m, vector<bool>& r) {
  if(!m) { debug("bug"); exit(1); }
  if(m->type == MT_NIL) {
    r.pb(false); r.pb(false);
  } else if(m->type == MT_CONS) {
    r.pb(true); r.pb(true);
    encode_impl(m->l, r);
    encode_impl(m->r, r);
  }else{
    if(m->value < 0) { r.pb(true); r.pb(false); }
    else { r.pb(false); r.pb(true); }
    encode_int(abs(m->value), r);
  }
}

vector<bool> encode_message(message_ptr const& m) {
  vector<bool> r;
  encode_impl(m, r);
  return r;
}

ZZ decode_int(vector<bool> const& v, int& i) {
  int sz = 0;
  int n = v.size();
  while(i < n && v[i]) {
    sz++;
    i++;
  }
  if(i == n) { debug(v,i); exit(1); }
  i++;
  ZZ r = 0;
  FOR(j, 4*sz) {
    if(i == n) { debug(v, i); exit(1); }
    r = 2*r + (v[i]?1:0);
    i++;
  }
  return r;
}

message_ptr decode_impl(vector<bool> const& v, int& i) {
  int n = v.size();
  if(i+2 > n) {
    debug(v, v.size(), i);
    exit(1);
  }
  if(v[i] == false && v[i+1] == false) {
    i += 2;
    return msg_nil();
  } else if(v[i] == true && v[i+1] == true) {
    i += 2;
    auto l = decode_impl(v, i);
    auto r = decode_impl(v, i);
    return msg_cons(l, r);
  } else if(v[i] == false && v[i+1] == true) {
    i += 2;
    ZZ r = decode_int(v, i);
    return msg_value(r);
  } else if(v[i] == true && v[i+1] == false) {
    i += 2;
    ZZ r = decode_int(v, i);
    return msg_value(-r);
  } else {
    exit(1);
  }
}

message_ptr decode_message(vector<bool> const& v) {
  int n = v.size();
  int i = 0;
  auto m = decode_impl(v, i);
  if(i != n) {
    debug("bug", v, i, n, m);
    exit(1);
  }
  return m;
}

string encode_message_string(message_ptr const& m){
  auto v = encode_message(m);
  string s;
  for(bool b : v) s.pb(b?'1':'0');
  return s;
}

message_ptr decode_message_string(string const& v) {
  vector<bool> b;
  for(char c : v) {
    if(c != '1' && c != '0') { debug(c); exit(1); }
    b.eb(c == '1');
  }
  return decode_message(b);
}

ZZ parse_int(string const& s, int &i) {
  int n = s.size();
  ZZ r = 0;
  while(i < n && '0' <= s[i] && s[i] <= '9') {
    r = 10*r+ZZ(s[i]-'0');
    i++;
  }
  return r;
}

struct parse_err{};
message_ptr parse_message_impl(string const& s, int &i) {
  int n = s.size();
  while(i < n && isspace(s[i])) i++;
  if(i == n) throw parse_err{};
	if (s[i] == '[') {
		i++;
    vector<message_ptr> cs;
    while(1) {
      while(i < n && isspace(s[i])) i++;
      if(i == n) throw parse_err{};
			if (s[i] == ')') {
				debug("bug"); exit(1);
			} else if(s[i] == ']') {
        i++;
        break;
      }else{
        cs.eb(parse_message_impl(s, i));
      }
    }
		return msg_list(cs);
	} else if(s[i] == '(') {
    i++;
    vector<message_ptr> cs;
    while(1) {
      while(i < n && isspace(s[i])) i++;
      if(i == n) throw parse_err{};
			if (s[i] == ']') {
				debug("bug"); exit(1);
			} if(s[i] == ')') {
        i++;
        break;
      }else{
        cs.eb(parse_message_impl(s, i));
      }
    }
    if(cs.size() == 0) return msg_nil();
    if(cs.size() == 1) return cs[0];
    if(cs.size() == 2) return msg_cons(cs[0], cs[1]);
    throw parse_err{};
  }else if(s[i] == '-') {
    i++;
    return msg_value(-parse_int(s, i));
  }else if('0' <= s[i] && s[i] <= '9') {
    return msg_value(parse_int(s, i));
  }else {
    throw parse_err{};
  }
}

message_ptr parse_message(string const& s) {
  try {
    int i = 0;
    int n = s.size();
    auto m = parse_message_impl(s, i);
    while(i < n && isspace(s[i])) i++;
    if(i != n) throw parse_err{};
    return m;
  }catch(parse_err) {
    return nullptr;
  }
}

message_ptr msg_list(vector<message_ptr> x) {
  message_ptr o = msg_nil();
  while(!x.empty()) {
    o = msg_cons(x.back(), o);
    x.pop_back();
  }
  return o;
}

vector<message_ptr> msg_unlist(message_ptr x) {
  vector<message_ptr> r;
  while(x->type == MT_CONS) {
    r.pb(x->l);
    x = x->r;
  }
  if(x->type != MT_NIL) { debug("bug"); exit(1); }
  return r;
}
