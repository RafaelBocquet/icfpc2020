#include "header.hpp"
#include "httplib.h"
#include "code_stream.hpp"

#include "run_client.hpp"

string serverUrl;
ZZ     playerKey;
string serverName;
int    serverPort;

unique_ptr<httplib::Client> client;

message_ptr post(message_ptr const& data) {
  string url = serverUrl + "/aliens/send";

  string str = encode_message_string(data);
  shared_ptr<httplib::Response> serverResponse = client->Post(url.c_str(), str.c_str(), "text/plain");

  if(!serverResponse) {
    cout << "Unexpected server response:\nNo response from server" << endl;
    exit(1);
  }

  if(serverResponse->status != 200) {
    cout << "Unexpected server response:\nHTTP code: " << serverResponse->status
         << "\nResponse body: " << serverResponse->body << endl;
    exit(2);
  }

  return decode_message_string(serverResponse->body);
}

int main(int argc, char** argv) {
  serverUrl = string(argv[1]);
  { istringstream ss(argv[2]); ss >> playerKey; }
  cout << "ServerUrl: " << serverUrl << "; PlayerKey: " << playerKey << endl;

  regex urlRegexp("http://(.+):(\\d+)");
  smatch urlMatches;
  if(!regex_search(serverUrl, urlMatches, urlRegexp) || urlMatches.size() != 3) {
    cout << "Unexpected server response:\nBad server URL" << endl;
    return 1;
  }

  serverName = urlMatches[1];
  serverPort = stoi(urlMatches[2]);
  client = make_unique<httplib::Client>(serverName, serverPort);
  client->set_connection_timeout(0, 3000000); // 300 milliseconds
  client->set_read_timeout(15, 0); // 5 seconds
  client->set_write_timeout(15, 0); // 5 seconds

  run_client(playerKey, post);

  return 0;
}
