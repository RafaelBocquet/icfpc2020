#include "parse_program.hpp"

bool is_name_char(char c) {
  return c == ':' || isalnum(c);
}

void skip_ws(string const& p, int& i) {
  int n = p.size();
  while(i < n && isspace(p[i])) i++;
}

string parse_name(string const& p, int& i) {
  int n = p.size();
  string name;
  while(i < n && is_name_char(p[i])) {
    name += p[i];
    i++;
  }
  if(name.empty()) { debug(p, i); exit(1); }
  return name;
}

static inline ZZ parse_int(string const& s, int &i) {
  int n = s.size();
  ZZ r = 0;
  while(i < n && '0' <= s[i] && s[i] <= '9') {
    r = 10*r+ZZ(s[i]-'0');
    i++;
  }
  return r;
}

expr_ptr parse_expr(string const& p, int& i) {
  skip_ws(p, i);
  if(p[i] == '-') {
    i++;
    ZZ v = parse_int(p, i);
    return expr_int(-v);
  }else if('0' <= p[i] && p[i] <= '9') {
    ZZ v = parse_int(p, i);
    return expr_int(v);
  }else{
    string name = parse_name(p, i);
    if(name == "ap") {
      auto f = parse_expr(p, i);
      auto x = parse_expr(p, i);
      return expr_app(f, x);
    }else{
      return expr_atom(name);
    }
  }
}

tuple<string, expr_ptr> parse_program_line(string const& p) {
  int n = p.size();
  int i = 0;
  skip_ws(p, i);
  if(i == n) return mt("", nullptr);

  auto name = parse_name(p, i);
  skip_ws(p, i);
  if(i == n || p[i] != '=') { debug(p, i); exit(1); }
  i++; // skip =
  skip_ws(p, i);
  auto e = parse_expr(p, i);
  skip_ws(p, i);
  if(i != n) { debug(p, i); exit(1); }
  return mt(name, e);
}
