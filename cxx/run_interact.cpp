#include "header.hpp"
#include "interact_options.hpp"
#include "parse_program.hpp"

#include "imgui/imgui.h"
#define IMGUI_DEFINE_MATH_OPERATORS
#include "imgui/imgui_internal.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include <GL/glew.h> // Initialize with glewInit()
// Include glfw3.h after our OpenGL definitions
#include <GLFW/glfw3.h>

#include "expr.hpp"
#include "send.hpp"

static void glfw_error_callback(int error, const char* description) {
  fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

interact_options O;

// Styling
const ImVec4 empty_color        = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
const ImVec4 filled_color1      = ImVec4(0.85f, 0.00f, 0.00f, 0.00f);
const ImVec4 filled_color2      = ImVec4(0.00f, 0.85f, 0.00f, 0.00f);
const ImVec4 filled_color3      = ImVec4(0.00f, 0.00f, 0.85f, 0.00f);
const ImVec4 filled_color4      = ImVec4(0.85f, 0.40f, 0.00f, 0.00f);
const ImVec4 filled_color5      = ImVec4(0.00f, 0.85f, 0.40f, 0.00f);
const ImVec4 filled_color6      = ImVec4(0.40f, 0.00f, 0.85f, 0.00f);
const ImVec4 filled_color7      = ImVec4(0.85f, 0.00f, 0.40f, 0.00f);
const ImVec4 filled_color8      = ImVec4(0.40f, 0.85f, 0.00f, 0.00f);
const ImVec4 filled_color9      = ImVec4(0.00f, 0.40f, 0.85f, 0.00f);
//const ImVec4 hover_empty_color  = ImVec4(0.00f, 0.50f, 0.00f, 1.00f);
//const ImVec4 hover_filled_color = ImVec4(0.65f, 0.85f, 0.65f, 1.00f);
const ImVec4 line_color         = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);

// Interaction state
message_ptr                  state;                // the current state
vector<set<tuple<ZZ, ZZ>>>   grid;                 // the output

// Recording state
bool                                is_recording;
vector<tuple<int,int>>              recorded_commands;
map<string, vector<tuple<int,int>>> saved_recordings;

// GUI State
int                          igrid1, igrid2, igrid3, igrid4, igrid5, igrid6, igrid7, igrid8, igrid9 = 0;            // the selected canvas
bool                         show_grid = false;
bool                         show_numbers = true;
bool                         force_number_color = true;
int                          interaction_step = 0;

// Some useful data
bool is_pixel_hovered = false;
int pixel_hovered_x, pixel_hovered_y;

void eval_file(string const& filename, message_ptr const& state0) {
  state = state0;
  if(!state) { debug("bug"); exit(1); }

  init_defs();
  ifstream in(filename);
  if(!in.good()) { debug(filename); exit(1); }
  string pline;
  vector<string> names;
  while(getline(in, pline)) {
    string name; expr_ptr e;
    tie(name, e) = parse_program_line(pline);
    //cout << name << " = " << e << endl;
    defname(name, e);
    names.eb(name);
  }
  eval(expr_atom("galaxy"));
}

message_ptr eval_galaxy(message_ptr const& x, message_ptr const& y) {
  auto g = eval(expr_atom("galaxy"));
  auto r = force_value(apply(apply(g, value_of_message(x)), value_of_message(y)));
  return message_of_value(r);
}

void run(message_ptr const& m123) {
  if(!m123 || m123->type != MT_CONS) { debug(m123); exit(1); }
  if(!m123->r || m123->r->type != MT_CONS) { debug(m123->r); exit(1); }
  if(!m123->r->r || m123->r->r->type != MT_CONS) { debug(m123->r->r); exit(1); }
  auto m1 = m123->l;
  if(!m1 || m1->type != MT_VALUE) { debug(m1); exit(1); }
  auto m2 = m123->r->l;
  auto m3 = m123->r->r->l;
  if(m1->value == 0) {
    // display something
    state = m2;
    //debug(state);
    auto encoded_grids = msg_unlist(m3);
    grid.assign(encoded_grids.size(), set<tuple<ZZ,ZZ>>());
    FOR(i, encoded_grids.size()) {
      auto points = msg_unlist(encoded_grids[i]);
      for(auto pt : points) {
        if(pt->type == MT_CONS && pt->l->type == MT_VALUE && pt->r->type == MT_VALUE) {
          ZZ x = pt->l->value;
          ZZ y = pt->r->value;
          grid[i].insert(mt(x, y));
        } else {
          debug("bug: bad point", pt);
          exit(1);
        }
      }
    }
  } else {
    cerr << "SEND " << m3 << endl;
    auto m3_ = send(m3);
    cerr << "RECV " << m3_ << endl;
    run(eval_galaxy(m2, m3_));
  }
}

void interact_at(int x, int y) {
  if(is_recording) recorded_commands.eb(x, y);
  run(eval_galaxy(state, msg_cons(msg_value(x), msg_value(y))));
  igrid1 = 0; igrid2 = 1; igrid3 = 2;
	igrid4 = 3; igrid5 = 4; igrid6 = 5;
	igrid7 = 6; igrid8 = 7; igrid9 = 8;
  interaction_step += 1;
}

bool number_at(int ig, ZZ x, ZZ y, ZZ& val) {
  if(grid[ig].count(mt(x,y))) return false;
  int sz1 = 0;
  while(grid[ig].count(mt(ZZ(x+1+sz1),y))) sz1 += 1;
  int sz2 = 0;
  while(grid[ig].count(mt(x,ZZ(y+1+sz2)))) sz2 += 1;
  if (grid[ig].count(mt(ZZ(x-1),ZZ(y-1)))) return false;
  if (grid[ig].count(mt(ZZ(x-1),ZZ(y+1+sz2)))) return false;
  if (grid[ig].count(mt(ZZ(x+1+sz1),ZZ(y-1)))) return false;
  if (grid[ig].count(mt(ZZ(x+1+sz1),ZZ(y+1+sz2)))) return false;
  bool ok = true;
  FOR (i, sz1+1) if (grid[ig].count(mt(ZZ(x+i),ZZ(y-1)))) ok = false;
  FOR (j, sz2+1) if (grid[ig].count(mt(ZZ(x-1),ZZ(y+j)))) ok = false;
  FOR (i, sz1+1) if (grid[ig].count(mt(ZZ(x+i),ZZ(y+1+sz2)))) ok = false;
  FOR (j, sz2+1) if (grid[ig].count(mt(ZZ(x+1+sz1),ZZ(y+j)))) ok = false;
  if (!ok) return false;
  if(sz1 >= 1 && (sz1 == sz2 || sz2 == sz1+1)) {
    val = 0;
    vector<bool> bits;
    FOR(i, sz2) FOR(j, sz2) bits.eb(grid[ig].count(mt(ZZ(x+1+j), ZZ(y+1+i))));
    FOR(i, sz2*sz2) if(bits[i]) val += (ZZ(1)<<i);
    if(sz2 == sz1+1) val = -val;
    return true;
  }
  return false;
}

void InteractionWindow(){
  ImGuiIO& io = ImGui::GetIO(); (void)io;

  static ImVec2 scrolling = ImVec2(0.0f, 0.0f); // point in the center of the grid ?
  static double zoom      = 20.0;

  if(io.MouseWheel > 0) zoom *= 1.15;
  if(io.MouseWheel < 0) zoom /= 1.15;

  if(zoom < 2.0)   zoom = 2.0;
  if(zoom > 200.0) zoom = 200.0;

  ImGui::Begin("Interaction");
  ImDrawList* draw_list = ImGui::GetWindowDrawList();
  ImVec2 p0 = ImGui::GetCursorScreenPos();
  ImVec2 grid_sz = ImGui::GetContentRegionAvail();
  ImGui::InvisibleButton("grid", grid_sz);

  ImVec2 center(grid_sz.x/2, grid_sz.y/2);

  auto grid_to_screen = [&](ImVec2 p) -> ImVec2 {
    return ImVec2(p0.x + center.x + scrolling.x + p.x * zoom, p0.y + center.y + scrolling.y + p.y * zoom);
  };
  auto screen_to_grid = [&](ImVec2 p) -> ImVec2 {
    return ImVec2((p.x - p0.x - center.x - scrolling.x)/zoom, (p.y - p0.y - center.y - scrolling.y)/zoom);
  };

  const int minx = screen_to_grid(ImVec2(0, 0)).x-3, maxx = screen_to_grid(grid_sz).x+3;
  const int miny = screen_to_grid(ImVec2(0, 0)).y-3, maxy = screen_to_grid(grid_sz).y+3;

  // Check the mouse status
  ImVec2 mouse_global = ImGui::GetIO().MousePos;
  // ImVec2 mouse_grid = ImVec2(mouse_global.x - p0.x, mouse_global.y - p0.y);

  bool grid_hovered = ImGui::IsItemHovered();
  is_pixel_hovered = false;

  // Draw the grid
  FORU(i, minx, maxx) {
    FORU(j, miny, maxy) {
      auto q0 = grid_to_screen(ImVec2(i, j));
      auto q1 = grid_to_screen(ImVec2(i+1, j+1));
      bool hovered = grid_hovered && (q0.x <= mouse_global.x && mouse_global.x <= q1.x &&
                                      q0.y <= mouse_global.y && mouse_global.y <= q1.y);

      if(hovered) {
        is_pixel_hovered = true;
        pixel_hovered_x = i;
        pixel_hovered_y = j;
      }

      bool pixel_value1 = (igrid1 < (int)grid.size()) ? grid[igrid1].count(mt(i,j)) : 0;
      bool pixel_value2 = (igrid2 < (int)grid.size()) ? grid[igrid2].count(mt(i,j)) : 0;
      bool pixel_value3 = (igrid3 < (int)grid.size()) ? grid[igrid3].count(mt(i,j)) : 0;
      bool pixel_value4 = (igrid4 < (int)grid.size()) ? grid[igrid4].count(mt(i,j)) : 0;
      bool pixel_value5 = (igrid5 < (int)grid.size()) ? grid[igrid5].count(mt(i,j)) : 0;
      bool pixel_value6 = (igrid6 < (int)grid.size()) ? grid[igrid6].count(mt(i,j)) : 0;
      bool pixel_value7 = (igrid7 < (int)grid.size()) ? grid[igrid7].count(mt(i,j)) : 0;
      bool pixel_value8 = (igrid8 < (int)grid.size()) ? grid[igrid8].count(mt(i,j)) : 0;
      bool pixel_value9 = (igrid9 < (int)grid.size()) ? grid[igrid9].count(mt(i,j)) : 0;
      ImVec4 color = empty_color;
      if (pixel_value1) color = color + filled_color1;
      if (pixel_value2) color = color + filled_color2;
      if (pixel_value3) color = color + filled_color3;
      if (pixel_value4) color = color + filled_color4;
      if (pixel_value5) color = color + filled_color5;
      if (pixel_value6) color = color + filled_color6;
      if (pixel_value7) color = color + filled_color7;
      if (pixel_value8) color = color + filled_color8;
      if (pixel_value9) color = color + filled_color9;
      if (hovered) color = color + ImVec4(0.2f, 0.2f, 0.2f, 0.0f);
      draw_list->AddRectFilled(q0, q1,
                               ImColor(color));

      if(hovered && ImGui::IsMouseClicked(ImGuiMouseButton_Left)) { // click
        interact_at(i, j);
      }
    }
  }

  if(show_grid) {
    FORU(i, minx, maxx+1) {
      draw_list->AddLine(grid_to_screen(ImVec2(i, miny)),
                         grid_to_screen(ImVec2(i, maxy+1)),
                         ImColor(line_color));
    }
    FORU(j, miny, maxy+1) {
      draw_list->AddLine(grid_to_screen(ImVec2(minx, j)),
                         grid_to_screen(ImVec2(maxx+1, j)),
                         ImColor(line_color));
    }
  }

  // Draw numbers
  if(show_numbers) {
    FOR(ig, grid.size()) if(ig == igrid1 || ig == igrid2 || ig == igrid3) {
      for(auto p : grid[ig]) {
        ZZ x,y; tie(x, y) = p;
        y--;

        ZZ val;
        if(number_at(ig, x, y, val)) {
          ImVec4 color = empty_color;
          if(ig == igrid1) color = color + filled_color1;
          if(ig == igrid2) color = color + filled_color2;
          if(ig == igrid3) color = color + filled_color3;
          if(ig == igrid4) color = color + filled_color4;
          if(ig == igrid5) color = color + filled_color5;
          if(ig == igrid6) color = color + filled_color6;
          if(ig == igrid7) color = color + filled_color7;
          if(ig == igrid8) color = color + filled_color8;
          if(ig == igrid9) color = color + filled_color9;
          color = color + ImVec4(0.3f, 0.3f, 0.3f, 0.0f);
					if (force_number_color) color = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
          string valStr = to_string(val);
          draw_list->AddText(NULL, 30.f, grid_to_screen(ImVec2((double)x, (double)y)), ImColor(color), valStr.c_str());
        }

      }
    }
  }

  if(grid_hovered && !ImGui::IsAnyItemActive() && ImGui::IsMouseDragging(ImGuiMouseButton_Right, 0.0f)) {
    scrolling = scrolling + io.MouseDelta;
  }

  ImGui::End();
}

void SettingsWindow(){
  ImGui::Begin("Settings");

  ImGui::SliderInt("Canvas index R", &igrid1, 0, grid.size()-1);
  ImGui::SliderInt("Canvas index G", &igrid2, 0, grid.size()-1);
  ImGui::SliderInt("Canvas index B", &igrid3, 0, grid.size()-1);

  ImGui::SliderInt("Canvas index 4", &igrid4, 0, grid.size());
  ImGui::SliderInt("Canvas index 5", &igrid5, 0, grid.size());
  ImGui::SliderInt("Canvas index 6", &igrid6, 0, grid.size());
  ImGui::SliderInt("Canvas index 7", &igrid7, 0, grid.size());
  ImGui::SliderInt("Canvas index 8", &igrid8, 0, grid.size());
  ImGui::SliderInt("Canvas index 9", &igrid9, 0, grid.size());


	{
    if(ImGui::IsKeyPressed(327)) if (igrid1 > 0) igrid1--;
    if(ImGui::IsKeyPressed(329)) if (igrid1 < (int)grid.size() - 1) igrid1++;
    if(ImGui::IsKeyPressed(324)) if (igrid2 > 0) igrid2--;
    if(ImGui::IsKeyPressed(326)) if (igrid2 < (int)grid.size() - 1) igrid2++;
    if(ImGui::IsKeyPressed(321)) if (igrid3 > 0) igrid3--;
    if(ImGui::IsKeyPressed(323)) if (igrid3 < (int)grid.size() - 1) igrid3++;
  }

  ImGui::Checkbox("show_grid", &show_grid);
  ImGui::Checkbox("show_numbers", &show_numbers);
	ImGui::Checkbox("force_number_color", &force_number_color);

  // Recording
  { static char recording_name[128] = "";
    ImGui::InputText("Recording name", recording_name, IM_ARRAYSIZE(recording_name));

    if(!is_recording && ImGui::Button("Start recording")) {
      is_recording = true;
    }
    if(is_recording && ImGui::Button("Stop recording")) {
      debug(recorded_commands);
      string name(recording_name);
      if(!name.empty() && !recorded_commands.empty()) {
        saved_recordings[name] = recorded_commands;
      }
      recorded_commands.clear();
      is_recording = false;
    }
  }

  // Replaying
  {
    vector<char const*> items;
    for(auto const& s : saved_recordings) items.pb(s.first.c_str());
    static int item_current = 0;
    ImGui::ListBox("Recorded commands", &item_current, items.data(), items.size(), 8);

    if(ImGui::Button("Replay")) {
      if(item_current >= 0 && item_current < (int)items.size()) {
        string name = items[item_current];
        auto commands = saved_recordings[name];
        for(auto p : commands) {
          int x, y; tie(x, y) = p;
          interact_at(x, y);
        }
      }
    }
  }

  // Informations
  { if(is_pixel_hovered) {
      ImGui::Text("pixel: (%d, %d)", pixel_hovered_x, pixel_hovered_y);
    }else{
      ImGui::Text("pixel: none");
    }

    int minx = (1)<<29;
    int maxx = -(1<<29);
    int miny = (1)<<29;
    int maxy = -(1<<29);

    FOR(ig, (int)grid.size()) {
      for(auto p : grid[ig]) {
        ZZ x,y; tie(x,y) = p;
        minx = min(minx, (int)x);
        maxx = max(maxx, (int)x);
        miny = min(miny, (int)y);
        maxy = max(maxy, (int)y);
      }
    }

    ImGui::Text("Min..Max coords : (%d..%d, %d..%d)", minx, maxx, miny, maxy);
  }

  ImGui::End();
}

void MemoryWindow() {
  ImGui::Begin("Memory");

  if(grid.size() >= 1 && ImGui::Button("Memory")) {
    vector<tuple<ZZ, ZZ, int, int, int, int>> MS;
    FOR(x1, 8) FOR(y1, 8) FOR(x2, 8) FOR(y2, 8) if(mt(x1,y1) < mt(x2,y2)) {
      // debug(x1, y1, x2, y2);
      ZZ val1, val2;
      if(number_at(0, x1*6, y1*6, val1) && number_at(0, x2*6, y2*6, val2)) {
        // debug(x1, y1, val1, x2, y2, val2);
        interact_at(6*x1,6*y1);
        interact_at(6*x2,6*y2);
        ZZ val3;
        if(!number_at(0, x1*6, y1*6, val3)) {
          MS.eb(val1, val2, x1, y1, x2, y2);
          debug("memory!", x1, y1, x2, y2, val1, val2);
        }
      }
    }
    for(auto p : MS) { debug(p); }
  }

  if(ImGui::Button("All Perms")) {
    int pairs[8][4] =
      {{1, 2, 12, 1},
       {14, 19, 8, 2},
       {43, 0, 33, 8},
       {25, 20, 2, 19},
       {43, 20, 7, 25},
       {26, 14, 14, 25},
       {14, 7, 13, 32},
       {7, 20, 20, 31}};
    FOR(mask, 1<<8) {
      interact_at(-75, -64);
      interact_at(-2, -65);
      FOR(i, 8) {
        if(mask&(1<<i)) {
          interact_at(pairs[i][0], pairs[i][1]);
          interact_at(pairs[i][2], pairs[i][3]);
        }else{
          interact_at(pairs[i][2], pairs[i][3]);
          interact_at(pairs[i][0], pairs[i][1]);
        }
      }
      interact_at(0, 0);
    }
    // 18 -75 -64 -2 -65
    //
    // 0 0
  }

  ImGui::End();
}

int main(int argc, char** argv) {
  O.parseArgs(argc, argv);
  { ifstream in("recordings.txt");
    if(in.good()) {
      string s;
      while(getline(in, s)) {
        istringstream ss(s);
        string name; ss >> name;
        if(name.empty()) continue;
        vector<tuple<int, int>> cmds;
        int nc; ss >> nc;
        FOR(j, nc) {
          int x,y; ss >> x >> y;
          cmds.eb(x, y);
        }
        saved_recordings[name] = cmds;
      }
    }
  }
  eval_file(O.program_file, O.base_state);

  // Setup window
  glfwSetErrorCallback(glfw_error_callback);
  if(!glfwInit()) return 1;

  // GL 3.0 + GLSL 130
  const char* glsl_version = "#version 130";
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
  //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only

  // Create window with graphics context
  GLFWwindow* window = glfwCreateWindow(1800, 950, "ICFPC2020", NULL, NULL);
  if(window == NULL) return 1;
  glfwMakeContextCurrent(window);
  glfwSwapInterval(1); // Enable vsync

  // Initialize OpenGL loader
  bool err = glewInit() != GLEW_OK;
  if(err) {
    fprintf(stderr, "Failed to initialize OpenGL loader!\n");
    return 1;
  }

  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO(); (void)io;
  //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
  //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

  // Setup Dear ImGui style
  ImGui::StyleColorsDark();
  //ImGui::StyleColorsClassic();

  // Setup Platform/Renderer bindings
  ImGui_ImplGlfw_InitForOpenGL(window, true);
  ImGui_ImplOpenGL3_Init(glsl_version);

  // Load Fonts
  // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
  // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
  // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
  // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
  // - Read 'docs/FONTS.md' for more instructions and details.
  // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
  //io.Fonts->AddFontDefault();
  //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
  //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
  //io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
  //io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
  //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
  //IM_ASSERT(font != NULL);

  // Our state
  ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
  bool show_demo_window = true;
  // bool show_another_window = false;

  // Main loop
  while(!glfwWindowShouldClose(window)) {
    // Poll and handle events (inputs, window resize, etc.)
    // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
    // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
    // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
    // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
    glfwPollEvents();

    // Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
    if(show_demo_window) {
      ImGui::ShowDemoWindow(&show_demo_window);
    }

    InteractionWindow();
    SettingsWindow();
    MemoryWindow();

    // Rendering
    ImGui::Render();
    int display_w, display_h;
    glfwGetFramebufferSize(window, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);
    glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwSwapBuffers(window);
  }

  // Cleanup
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();

  glfwDestroyWindow(window);
  glfwTerminate();

  { ofstream out("tmp_recordings.txt");
    for(auto p : saved_recordings) {
      out << p.first << ' ';
      out << p.second.size();
      for(auto q : p.second) {
        int x,y; tie(x,y) = q; out << ' ' << x << ' ' << y;
      }
      out << endl;
    }
  }

  return 0;
}
