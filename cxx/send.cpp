#include "header.hpp"
#include "code_stream.hpp"
#include "send.hpp"
#include <boost/process.hpp>

message_ptr send(message_ptr const& m) {
  if(!m) {
    debug("No message");
    exit(1);
  }

  string msgString = encode_message_string(m);

  boost::process::ipstream out;
  boost::process::ipstream err;
  boost::process::child curl(boost::process::search_path("curl"),
                             "-s",
                             "-X", "POST",
                             "https://icfpc2020-api.testkontur.ru/aliens/send?apiKey=e1e8bc748f9440c2a2a73b5da0e1338e",
                             "-H", "Content-Type: text/plain",
                             "-d", msgString,
                             boost::process::std_out > out);
  string answer;
  out >> answer;
  curl.wait();
  return decode_message_string(answer);
}

