#pragma once
#include "header.hpp"
#include "code_stream.hpp"

struct interact_options {
  void parseArgs(int argc, char** argv);

  string program_file;
  message_ptr base_state;
};
