#include "header.hpp"
#include "code_stream.hpp"

// const int MAX_TURN = 2048 + 32; // adding 32 to be safe

struct vec2 {
  int x, y;

  vec2 operator-() const { return vec2{-x,-y}; }
  vec2 operator+(vec2 const& o) const { return vec2{x+o.x,y+o.y}; }
  vec2 operator-(vec2 const& o) const { return vec2{x-o.x,y-o.y}; }
  vec2& operator+=(vec2 const& o) { x+=o.x; y+=o.y; return *this; }
  vec2& operator-=(vec2 const& o) { x-=o.x; y-=o.y; return *this; }

  bool operator==(vec2 const& o) const { return tie(x, y) == tie(o.x,o.y); }
  bool operator<(vec2 const& o) const { return tie(x, y) < tie(o.x,o.y); }
  bool operator<=(vec2 const& o) const { return tie(x, y) <= tie(o.x,o.y); }

  int dist(vec2 const& o) const {
    return max(abs(x-o.x), abs(y-o.y));
  }
};

static inline ostream& operator<<(ostream& out, vec2 const& p) {
  return out << mt(p.x,p.y);
}

struct ship_stats {
  int health;
  int attack;
  int cooling;
  int clones;
};

struct ship {
  int        id;
  int        owner;
  vec2       pos;
  vec2       spd;
  ship_stats stats;

  int heat;

  int heat_cap;
  int move_speed;

  vec2 new_pos, new_spd;

  int score();
};

struct game_static {
  int  max_turn;

  int  defending;
  int  max_cost;

  int  planet_radius;
  int  world_radius;

  // the stats of the def player (if we are the atk player)
  ship_stats def_base_stats;
};

enum COMMAND_TYPE
{ CMD_MOVE,
  CMD_EXPLODE,
	CMD_FIRE,
	CMD_SPLIT
};

struct command {
  COMMAND_TYPE type;
  int          ship_id;
  vec2         move_delta;

	vec2         fire_pos;
	int          fire_power;

	ship_stats   child_stats;
};

static inline ostream& operator<<(ostream& out, ship_stats const& s) {
	out << "(health=" << s.health << ", attack=" << s.attack << ", cooling=" << s.cooling << ", clones=" << s.clones << ")";
	return out;
}

static inline ostream& operator<<(ostream& out, command const& c) {
	if(c.type == CMD_MOVE) out << "(MOVE " << c.ship_id << " " << c.move_delta << ")";
	else if(c.type == CMD_EXPLODE) out << "(EXPLODE " << c.ship_id << ")";
	else if(c.type == CMD_FIRE) out << "(FIRE " << c.ship_id << " " << c.fire_pos << " " << c.fire_power << ")";
	else if(c.type == CMD_SPLIT) out << "(SPLIT " << c.ship_id << " " << c.child_stats << ")";
	return out;
}

static inline command cmd_move(int ship_id, vec2 move_delta) {
  command c; c.type = CMD_MOVE; c.ship_id = ship_id; c.move_delta = -move_delta;
  return c;
}

static inline command cmd_explode(int ship_id) {
  command c; c.type = CMD_EXPLODE; c.ship_id = ship_id;
  return c;
}

static inline command cmd_fire(int ship_id, vec2 fire_pos, int fire_power) {
  command c; c.type = CMD_FIRE; c.ship_id = ship_id; c.fire_pos = fire_pos; c.fire_power = fire_power;
  return c;
}

static inline command cmd_split(int ship_id, ship_stats child_stats) {
  command c; c.type = CMD_SPLIT; c.ship_id = ship_id; c.child_stats = child_stats;
  return c;
}


extern game_static gw;
extern int cur_turn;
extern vector<ship> ships;
ship_stats start_ai();
vector<command> run_ai();
