#include "run_client.hpp"
#include "ai.hpp"

ship_stats parse_ship_stats(message_ptr m) {
  vector<message_ptr> ms = msg_unlist(m);
  assert(ms.size() == 4);
  return ship_stats { (int)ms[0]->value, (int)ms[1]->value, (int)ms[2]->value, (int)ms[3]->value };
}

vec2 parse_vec2(message_ptr m){
  return vec2 { (int)m->l->value, (int)m->r->value };
}

ship parse_ship(message_ptr m) {
  vector<message_ptr> ms = msg_unlist(m);
  assert(ms.size() == 8);
  ship s;
  s.owner      = (int)ms[0]->value;
  s.id         = (int)ms[1]->value;
  s.pos        = parse_vec2(ms[2]);
  s.spd        = parse_vec2(ms[3]);
  s.stats      = parse_ship_stats(ms[4]);
  s.heat       = (int)ms[5]->value;
  s.heat_cap   = (int)ms[6]->value;
  s.move_speed = (int)ms[7]->value;
  return s;
}

void parse_gw(message_ptr m) {
  vector<message_ptr> ms = msg_unlist(m);
  assert(ms.size() == 5);

  gw.max_turn = (int)ms[0]->value + 8;
  gw.defending = (int)ms[1]->value;

  vector<message_ptr> cs = msg_unlist(ms[2]);
  gw.max_cost = (int)cs[0]->value;

  vector<message_ptr> ss = msg_unlist(ms[3]);
  gw.planet_radius = (int)ss[0]->value;
  gw.world_radius = (int)ss[1]->value;

  if(!gw.defending) gw.def_base_stats = parse_ship_stats(ms[4]);
}

void parse_game_state(message_ptr m) {
  vector<message_ptr> ms = msg_unlist(m);

  cur_turn = (int)ms[0]->value;
  cout << endl;
  cout << "Turn: " << cur_turn << endl;

  ships.clear();
  vector<message_ptr> us = msg_unlist(ms[2]);
  for(auto const& u : us) {
    auto ship = parse_ship(u->l);
    ships.pb(ship);
  }
}

vector<ZZ> tokens = { 103652820ll,
192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192495633910ll, 192497804470ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll, 192496425430ll, 192498595990ll
};

message_ptr makeJoinRequest(ZZ playerKey) {
  vector<message_ptr> mtokens;
  for(auto t : tokens) mtokens.eb(msg_value(t));
  return msg_list({msg_value(2), msg_value(playerKey), msg_list(mtokens)});
}

message_ptr encode_ship_stats(ship_stats s) {
  return msg_list({msg_value(s.health), msg_value(s.attack), msg_value(s.cooling), msg_value(s.clones)});
}

// TODO: find ok start requests
message_ptr makeStartRequest(ZZ playerKey, ship_stats s) {
  return msg_list({msg_value(3),msg_value(playerKey), encode_ship_stats(s)});
}

message_ptr encode_vec2(vec2 p) { return msg_cons(msg_value(p.x), msg_value(p.y)); }

message_ptr encode_command(command c) {
  if(c.type == CMD_MOVE) { return msg_list({msg_value(0), msg_value(c.ship_id), encode_vec2(c.move_delta)}); }
  else if(c.type == CMD_EXPLODE) { return msg_list({msg_value(1), msg_value(c.ship_id)}); }
	else if(c.type == CMD_FIRE) {
		return msg_list({msg_value(2), msg_value(c.ship_id), encode_vec2(c.fire_pos), msg_value(c.fire_power)});
	} else if(c.type == CMD_SPLIT) {
		return msg_list({msg_value(3), msg_value(c.ship_id), encode_ship_stats(c.child_stats)});
	} else { debug("bug"); exit(1); }
}

message_ptr makeCmdRequest(ZZ playerKey, vector<command> const& cs) {
  vector<message_ptr> mcs;
  for(auto c : cs) mcs.eb(encode_command(c));
  return msg_list({msg_value(4), msg_value(playerKey), msg_list(mcs)});
}

// [1 0] : explode id 0
// [0 0 (2 -2)] : move id 0 by speed
// TODO: fire and split

void run_client(ZZ playerKey, function<message_ptr(message_ptr const&)> post) {
  vector<message_ptr> gameResponse;
  gameResponse = msg_unlist(post(makeJoinRequest(playerKey)));
  parse_gw(gameResponse[2]);
  debug(gameResponse);

  ship_stats start_stats = start_ai();

  gameResponse = msg_unlist(post(makeStartRequest(playerKey, start_stats)));
  debug(gameResponse);

  // gameResponse[2] :
  //   [ gameLength (256)
  //   , playerId (0 = def, 1 = atk) TODO check which is which
  //   , [512 2 64] // 512: sum of costs must be less or equal
  //                        weights: [1, 4, 12, 2]
  //                //  2: move distance (always 2 for use, lets assume 2 for opponent)
  //                // 64: cooler capacity ?
  //   , [16 128] // planet radius, game zone radius
  //   , [x y z w] // atk stats if defs, empty list otherwise
  //   ]
  // gameResponse[3]:
  //   [ turnCount
  //   , [16 128] // still radiuses... can these change ?
  //   // list of ships (by owner)
  //   //    ID?  OWNER?     POSITION  SPEED  STATS          CURRENT COOLER       COOLER CAPACITY???   MOVE SPEED???
  //   , [ [ [1     0      (-48 30)  (0 0)  [326 0 10 1]        0                      64                2]
  //         [] ---- previous commands (last turn) !!! (empty the first turn)
  //       ]
  //     , [ [0 1 (48 -30) (0 0) [326 0 10 1] 0 64 2]
  //         []
  //       ]
  //     ]
  //   ]

  while(gameResponse[1]->value != 2) {
    parse_game_state(gameResponse[3]);

    chrono::steady_clock::time_point time_start = chrono::steady_clock::now();

    auto cs = run_ai();

    chrono::steady_clock::time_point time_end   = chrono::steady_clock::now();
    double elapsed = chrono::duration_cast<chrono::milliseconds>(time_end - time_start).count();

    cout << endl;
    cout << "Commands: " << cs << endl;
    cout << "Elapsed: " << elapsed << " ms" << endl;

    gameResponse = msg_unlist(post(makeCmdRequest(playerKey, cs)));
    debug(gameResponse);
  }
}
