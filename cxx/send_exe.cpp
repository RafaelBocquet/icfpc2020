#include "header.hpp"
#include "code_stream.hpp"
#include "send.hpp"

int main(int argc, char** argv) {
  if(argc != 2) {
    cerr << "USAGE: send \"[MESSAGE]\"" << endl;
    return 1;
  }
  string input = argv[1];
  message_ptr msg = parse_message(input);
  if(!msg) {
    cerr << "Can't parse message: " << input << endl;
    exit(1);
  }

  auto answer = send(msg);
  cout << answer << endl;

  return 0;
}
