#include "expr.hpp"
#include "code_stream.hpp"

map<string, value_ptr> definitions;

value_ptr eval(expr_ptr e) {
	if (e->type == ET_INT) {
		value v; v.type = VT_INT; v.intvalue = e->intvalue;
		return make_shared<value>(v);
	} else if (e->type == ET_ATOM) {
		if(!definitions.count(e->atomvalue)) {
			debug(e);
			exit(1);
		}
		return force_value(definitions[e->atomvalue]);
	}
	value_ptr f = eval(e->l);
	return apply(f, value_lazy(e->r));
}

value_ptr force_value(value_ptr x) {
	if (x->type == VT_FORWARD) {
		return x->val1;
	} else if (x->type == VT_LAZY) {
		assert (x->e);
		expr_ptr e = x->e;
		//debug(e);
		x->e = nullptr;
		value_ptr v = force_value(eval(e));
		x->type = VT_FORWARD;
		x->val1 = v;
		return v;
	} else if (x->type == VT_LAZYAPP) {
		assert (x->val1 && x->val2);
		value_ptr f = x->val1;
		value_ptr y = x->val2;
		//debug(f, y);
		x->val1 = nullptr;
		x->val2 = nullptr;
		value_ptr v = force_value(apply(f, y));
		x->type = VT_FORWARD;
		x->val1 = v;
		return v;
	} else {
		return x;
	}
}

value_ptr apply(value_ptr f, value_ptr x) {
	f = force_value(f);
	//debug(f, x);
	switch (f->type) {
		case VT_INT: {
			if(f->intvalue < 0) {
				debug("bug: apply int negative", f->intvalue); exit(1);
			}
			return value_int1(VT_INT1, f->intvalue, x);
		}

		case VT_INT1: {
			if(f->intvalue < 0) {
				debug("bug: apply int negative", f->intvalue); exit(1);
			}
			// church numeral
			for(ZZ cnt = f->intvalue; cnt != 0; --cnt) {
				x = apply(f->val1, x);
			}
			return x;
		}

		case VT_LAZY: case VT_LAZYAPP: {
			debug("bug: apply lazy"); exit(1);
		}
		case VT_FORWARD: {
			debug("bug: apply forward"); exit(1);
		}

			// by definition: nil x = true
		case VT_NIL: {
			return value_0(VT_TRUE);
		}

		case VT_SUCC: {
			x = force_value(x);
			assert (x->type == VT_INT);
			return value_intlike(VT_INT, x->intvalue + ZZ(1));
		}

		case VT_PRED: {
			x = force_value(x);
			assert (x->type == VT_INT);
			return value_intlike(VT_INT, x->intvalue - ZZ(1));
		}

		case VT_ADD: {
			// x = force_value(x);
			// assert (x->type == VT_INT);
			return value_1(VT_ADD1, x);
		}

		case VT_ADD1: {
			x = force_value(x);
			value_ptr y = force_value(f->val1);
			assert (y->type == VT_INT);
			assert (x->type == VT_INT);
			return value_intlike(VT_INT, y->intvalue + x->intvalue);
		}

		case VT_MUL: {
			//x = force_value(x);
			//assert (x->type == VT_INT);
			return value_1(VT_MUL1, x);
		}

		case VT_MUL1: {
			x = force_value(x);
			value_ptr y = force_value(f->val1);
			assert (y->type == VT_INT);
			assert (x->type == VT_INT);
			return value_intlike(VT_INT, y->intvalue * x->intvalue);
		}

		case VT_FLOORDIV: {
			// x = force_value(x);
			//assert (x->type == VT_INT);
			return value_1(VT_FLOORDIV1, x);
		}

		case VT_FLOORDIV1: {
			x = force_value(x);
			value_ptr y = force_value(f->val1);
			assert (y->type == VT_INT);
			assert (x->type == VT_INT);
			return value_intlike(VT_INT, y->intvalue / x -> intvalue);
		}

		case VT_NEG: {
			x = force_value(x);
			assert (x->type == VT_INT);
			return value_intlike(VT_INT, - x->intvalue);
		}

		case VT_2POW: {
			x = force_value(x);
			assert (x->type == VT_INT);
			return value_intlike(VT_INT, ZZ(1) << (int)x->intvalue);
		}

		case VT_EQ: {
			//x = force_value(x);
			//assert (x->type == VT_INT);
			return value_1(VT_EQ1, x);
		}

		case VT_EQ1: {
			x = force_value(x);
			value_ptr y = force_value(f->val1);
			assert (y->type == VT_INT);
			assert (x->type == VT_INT);
			bool b = y->intvalue == x->intvalue;
			return value_0(b ? VT_TRUE : VT_FALSE);
		}

		case VT_LT: {
			//x = force_value(x);
			//assert (x->type == VT_INT);
			return value_1(VT_LT1, x);
		}

		case VT_LT1: {
			x = force_value(x);
			value_ptr y = force_value(f->val1);
			assert (y->type == VT_INT);
			assert (x->type == VT_INT);
			bool b = y->intvalue < x->intvalue;
			return value_0(b ? VT_TRUE : VT_FALSE);
		}

		case VT_PAIR: {
			return value_1(VT_PAIR1, x);
		}
		case VT_PAIR1: {
			return value_2(VT_PAIR2, f->val1, x);
		}
		case VT_PAIR2: {
			return apply(apply(x, f->val1), f->val2);
		}

		case VT_S: {
			return value_1(VT_S1, x);
		}
		case VT_S1: {
			return value_2(VT_S2, f->val1, x);
		}
		case VT_S2: {
			return apply(apply(f->val1, x), value_2(VT_LAZYAPP, f->val2, x));
		}

		case VT_C: {
			return value_1(VT_C1, x);
		}
		case VT_C1: {
			return value_2(VT_C2, f->val1, x);
		}
		case VT_C2: {
			return apply(apply(f->val1, x), f->val2);
		}

		case VT_B: {
			return value_1(VT_B1, x);
		}
		case VT_B1: {
			return value_2(VT_B2, f->val1, x);
		}
		case VT_B2: {
			return apply(f->val1, value_2(VT_LAZYAPP, f->val2, x));
		}

		case VT_I: {
			return x;
		}

		case VT_TRUE: {
			return value_1(VT_TRUE1, x);
		}
		case VT_TRUE1: {
			return f->val1;
		}

		case VT_FALSE: {
			return value_1(VT_FALSE1, x);
		}
		case VT_FALSE1: {
			return x;
		}

		case VT_ISNIL: {
			x = force_value(x);
			if (x->type == VT_NIL) {
				return value_0(VT_TRUE);
			} else {
				return value_0(VT_FALSE);
			}
			// else {
			// 	debug("bug: isnil"); exit(1);
			// }
		}

		case VT_CAR: {
			x = force_value(x);
			if (x->type == VT_PAIR2) {
				return x->val1;
			} else {
				debug("bug: car"); exit(1);
			}
		}

		case VT_CDR: {
			x = force_value(x);
			if (x->type == VT_PAIR2) {
				return x->val2;
			} else {
				debug("bug: cdr"); exit(1);
			}
		}

		default: {
			debug("bug: unknown value type"); exit(1);
		}

	}
}

message_ptr message_of_value(value_ptr v) {
	v = force_value(v);
	switch (v->type) {
		case VT_INT: {
			message m; m.type = MT_VALUE; m.value = v->intvalue; return make_shared<message>(m);
		}

		case VT_NIL: {
			message m; m.type = MT_NIL; return make_shared<message>(m);
		}

		case VT_PAIR2: {
			message m; m.type = MT_CONS;
			m.l = message_of_value(v->val1);
			m.r = message_of_value(v->val2);
			return make_shared<message>(m);
		}

		default: {
			debug("bug: value not compatible with message", v); exit(1);
		}
	}
}

value_ptr value_of_message(message_ptr m) {
	switch (m->type) {
		case MT_VALUE: {
			return value_intlike(VT_INT, m->value);
		}

		case MT_NIL: {
			return value_0(VT_NIL);
		}

		case MT_CONS: {
			return value_2(VT_PAIR2, value_of_message(m->l), value_of_message(m->r));
		}

		default: {
			debug("bug: unknown message type"); exit(1);
		}
	}
}

void defprim(string name, VALUE_TYPE type) {
	value v; v.type = type;
	definitions[name] = make_shared<value>(v);
}

void defname(string const& name, expr_ptr e){
	definitions[name] = value_lazy(e);
}

void init_defs() {
	defprim("inc", VT_SUCC);
	defprim("dec", VT_PRED);
	defprim("add", VT_ADD);
	defprim("mul", VT_MUL);
	defprim("div", VT_FLOORDIV);
	defprim("eq", VT_EQ);
	defprim("lt", VT_LT);
	defprim("neg", VT_NEG);
	defprim("s", VT_S);
	defprim("c", VT_C);
	defprim("b", VT_B);
	defprim("i", VT_I);
	defprim("t", VT_TRUE);
	defprim("f", VT_FALSE);
	defprim("pwr2", VT_2POW);
	defprim("cons", VT_PAIR);
	defprim("car", VT_CAR);
	defprim("cdr", VT_CDR);
	defprim("nil", VT_NIL);
	defprim("isnil", VT_ISNIL);
}
