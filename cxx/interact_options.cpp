#include "interact_options.hpp"
#include "args.hxx"
#include "send.hpp"

void interact_options::parseArgs(int argc, char** argv) {
  args::ArgumentParser parser("icfpc2020");
  args::HelpFlag help(parser, "help", "Display available options", {'h', "help"});

  args::ValueFlag<string> program_file_(parser, "program_file", "The program file to interpret/interact with (e.g. galaxy.txt).", {"prg"});
  args::ValueFlag<string> base_state_(parser, "base_state", "The base state of the interaction.", {"st"});
  args::ValueFlag<string> replay_(parser, "", "", {"replay"});

  try {
    parser.ParseCLI(argc, argv);
  } catch (const args::Completion& e) {
    cout << e.what();
    exit(0);
  } catch (const args::Help&) {
    cout << parser;
    exit(0);
  } catch (const args::ParseError& e) {
    cerr << e.what() << endl;
    cerr << parser;
    exit(1);
  }

  program_file = args::get(program_file_);
  if(base_state_) base_state = parse_message(args::get(base_state_));
  if(replay_) {
    istringstream ss(args::get(replay_));
    ZZ replayI; ss>>replayI;
    auto replay = send(msg_list({msg_value(5), msg_value(replayI)}));
    base_state = msg_list({msg_value(5), msg_list({msg_value(7), msg_value(replayI), msg_nil(), msg_nil(), msg_nil(), replay, msg_cons(msg_value(-18), msg_value(0)), msg_value(0)}), msg_value(9), msg_list({})});
    // [5 [7 $1 [] [] [] $r (-18 0) 55629] 9 [103652820 192495633910]]
  }
}
