#include "header.hpp"
#include "send.hpp"
#include "code_stream.hpp"

#include "run_client.hpp"

int main(int argc, char** argv) {
  assert(argc == 2);
  ZZ playerKey; { istringstream is(argv[1]); is >> playerKey; }
  cout << "playerKey: " << playerKey << endl;
  run_client(playerKey, send);

  return 0;
}
