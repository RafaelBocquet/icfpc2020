#pragma once
#include "header.hpp"
#include "expr.hpp"

tuple<string, expr_ptr> parse_program_line(string const& program);
