#pragma once
#include "header.hpp"

enum MESSAGE_TYPE
{ MT_VALUE,
  MT_NIL,
  MT_CONS
};

struct message;
using message_ptr = shared_ptr<message>;
struct message {
  MESSAGE_TYPE type;
  ZZ           value;
  message_ptr  l, r;
};

static inline bool is_list(message_ptr m) {
	while (1) {
		if(!m) { debug("bug"); exit(1); }
		if(m->type == MT_NIL) return true;
		if(m->type == MT_VALUE) return false;
		assert (m->type == MT_CONS);
		m = m->r;
	}
}
static inline void show_message_not_list(ostream &out, message_ptr m);
static inline void show_message_list(ostream &out, message_ptr m);
static inline ostream& operator<<(ostream& out, message_ptr const& m) {
  if(!m) { debug("bug"); exit(1); }
	if (is_list(m)) show_message_list(out, m); else show_message_not_list(out, m);
	return out;
}
static inline void show_message_not_list(ostream &out, message_ptr m) {
  if(!m) { debug("bug"); exit(1); }
  if(m->type == MT_NIL) out << "()";
  else if(m->type == MT_CONS) {
		out << "(" << m->l << " ";
		show_message_not_list(out, m->r);
		out << ")";
	} else if(m->type == MT_VALUE) out << m->value;
}
static inline void show_message_list(ostream &out, message_ptr m) {
  if(!m) { debug("bug"); exit(1); }
	if(m->type == MT_NIL) {
		out << "[]";
		return;
	}
	out << "["; out << m->l; m = m->r;

	while (1) {
		if(!m) { debug("bug"); exit(1); }
		if (m->type == MT_NIL) {
			out << "]"; return;
		}
		assert (m->type == MT_CONS);
		out << " " << m->l;
		m = m->r;
	}
}


vector<bool> encode_message(message_ptr const& m);
message_ptr decode_message(vector<bool> const& v);
string encode_message_string(message_ptr const& m);
message_ptr decode_message_string(string const& v);

message_ptr parse_message(string const& s);

static inline message_ptr msg_nil() {
  message m; m.type = MT_NIL;
  return make_shared<message>(m);
}

static inline message_ptr msg_cons(message_ptr l, message_ptr r) {
  message m; m.type = MT_CONS; m.l = l; m.r = r;
  return make_shared<message>(m);
}

static inline message_ptr msg_value(ZZ value) {
  message m; m.type = MT_VALUE; m.value = value;
  return make_shared<message>(m);
}

message_ptr msg_list(vector<message_ptr> x);
vector<message_ptr> msg_unlist(message_ptr x);
